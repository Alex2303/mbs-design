/*
* PrivateShop
*
* Do not edit or add to this file.
* You are not authorized to modify, copy or redistribute this file.
* Permissions are reserved by FME Modules.
*
*  @author    FME Modules
*  @copyright 2020 FME Modules All right reserved
*  @license   FME Modules
*  @category  FMM Modules
*  @package   PrivateShop
*/

// load() event and resize() event are combined
$(window).ready(responsiveFn).resize(responsiveFn);

$(function(){
    setTimeout(function(){
        $.uniform.restore(".no-uniform");
        //$(".no-uniform").not('.noUniform').uniform();
    }, 500);
});

function SignUp()
{
    $('.alert-danger, .error').hide();
    $('#private-login').hide();
    $('#new-private-account').show();
    $('.alert').addClass('private_error_resp');
}

function Login()
{
    $('.alert-danger, .error').hide();
    $('#private-login').show();
    $('#new-private-account').hide();
    $('.alert').addClass('private_error_resp');
}

function forgot_password()
{
    //$('#ps17_errors').html('').hide();
    $('#private-login').hide();
    $('#private-lost-password').show();
    $('.alert').addClass('private_error_resp');
}

function BackToLogin()
{
    if (typeof baseUri === 'undefined') {
        baseUri = prestashop.urls.base_url;
    }

    if (getUrlParameter('reset_token') !== '') {
        window.location.replace(baseUri);
    } else {
        $('.private_error_resp').html('').hide();
        $('#private-lost-password').hide();
        $('#private-login').fadeIn('slow');
    }
}

function getUrlParameter(name)
{
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function responsiveFn()
{
    width = $( window ).width();
    height = $( window ).height();

    if(width <= 320) {
        $('#login_form').removeClass('box');
    }
    else {
        $('#login_form').addClass('box');
    }
}

function toggleVis(e) {
    var elm = $(e).closest('.input-group').children('input.js-visible-password');
    if (elm.attr('type') === 'password') {
        elm.attr('type', 'text');
        $(e).text($(e).data('textHide'));
    } else {
        elm.attr('type', 'password');
        $(e).text($(e).data('textShow'));
    }

}

function registerNewUser(el) {
    var gif_loader = $('#gif_loader');
    gif_loader.show();
    var form_data = $('#new-private-account form').serialize();
    var requestData = {
        type : "POST",
        cache : false,
        url : ajax_url,
        dataType : "json",
        data : form_data,
        success: function(jsonData) {
            if (jsonData.errors > 0) {
                var __html = '<div class="alert alert-danger" id="ps17_errors"><ol><li>' + jsonData.html + '</li></ol></div>';
                $('#error_holder').html(__html);
                gif_loader.hide();
            } else {
                $('#error_holder').html('');
                if (jsonData.redirect === true) {
                    window.location = jsonData.redirect_url;
                } else {
                    $('#new-private-account').html(jsonData.message);
                }

                $('#private-back').show();
                gif_loader.hide();
            }
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            gif_loader.hide();
        }
    };
    $.ajax(requestData);
}

function setBGvideo(value) {
    var wrapper = (psNew)? '#wrapper' : '#center_column';
    $(wrapper).tubular({videoId: value});
}

$(document).on('click', '#SubmitLogin', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    //var gif_loader = $('#gif_loader');
    //gif_loader.show();
    var jsonData = {
        type : "POST",
        cache : false,
        url : ajax_url,
        dataType : "json",
        data : {
            action : 'privateLogin',
            ajax : true,
            email : $.trim($('input[name=email]').val()),
            passwd : $.trim($('input[name=passwd]').val())
        },
        success: function(response) {
            if (response.errors > 0) {
                var __html = '<div class="alert alert-danger" id="ps17_errors"><ol><li>'+response.html+'</li></ol></div>';
                $('#error_holder').html(__html);
                //gif_loader.hide();
            } else if (response.success) {
                $('#error_holder').html('');
                //gif_loader.hide();
                window.location.reload();
            }
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(textStatus + '<br>' + errorThrown);
            //gif_loader.hide();
        }
    };
    $.ajax(jsonData);
});

