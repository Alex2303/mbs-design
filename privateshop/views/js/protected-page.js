/*
* PrivateShop
*
* Do not edit or add to this file.
* You are not authorized to modify, copy or redistribute this file.
* Permissions are reserved by FME Modules.
*
*  @author    FME Modules
*  @copyright 2020 FME Modules All right reserved
*  @license   FME Modules
*  @category  FMM Modules
*  @package   PrivateShop
*/

$(document).on('submit', '#private-page', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var jsonData = {
        type : "POST",
        cache : false,
        url : $(this).attr('action'),
        dataType : "json",
        data : {
            action : 'protectedAccess',
            ajax : true,
            passwd : $.trim($('input[name=password]').val())
        },
        success: function(response) {
            console.log(response)
            if (response.success == false && response.errors != '') {
                var __html = '<div class="alert alert-danger"><ol><li>' + response.errors + '</li></ol></div>';
                $('#protect-access-error').html(__html);
            } else if (response.success) {
                $('#protect-access-error').html('');
                window.location.reload();
                //location.href = response.redirect_url;
            }
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(textStatus + '<br>' + errorThrown);
            //gif_loader.hide();
        }
    };
    $.ajax(jsonData);
});





