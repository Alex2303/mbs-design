{*
* DISCLAIMER
*
* Do not edit or add to this file.
* You are not authorized to modify, copy or redistribute this file.
* Permissions are reserved by FME Modules.
*
*  @author    FMM Modules
*  @copyright FME Modules 2020
*  @license   Single domain
*}

{extends file="helpers/form/form.tpl"}

{block name="input"}
    {if $input.type == 'cms_pages'}
      <div id="type_cms" class="panel col-lg-7">
          <table class="table">
            <thead>
              <tr>
                <th>
                  <input type="checkbox"
                  name="checkme"
                  id="checkme-cms"
                  class="noborder"
                  onclick="checkDelBoxes(this.form, '{$input.name|escape:'htmlall':'UTF-8'}', this.checked)" />
                </th>
                <th>{l s='ID' mod='privateshop'}</th>
                <th>{l s='Name' mod='privateshop'}</th>
              </tr>
            </thead>
            <tbody>
              {foreach $input.values as $key => $page}
                <tr {if $key%2}class="alt_row"{/if}>
                  <td>
                    <input type="checkbox"
                    class="cmsBox"
                    name="cms[]"
                    id="{$page.id_cms|escape:'htmlall':'UTF-8'}"
                    value="{$page.id_cms|escape:'htmlall':'UTF-8'}"
                    {if isset($fields_value.cms) AND in_array($page.id_cms, $fields_value.cms)}checked="checked"{/if} />
                  </td>
                  <td class="fixed-width-xs">
                    {$page.id_cms|escape:'htmlall':'UTF-8'}
                  </td>
                  <td>
                    <label class="control-label" for="{$page.id_cms|escape:'htmlall':'UTF-8'}">
                      {$page.meta_title|escape:'htmlall':'UTF-8'}
                    </label>
                  </td>
                </tr>
              {/foreach}
            </tbody>
          </table>
      </div>
      <div class="clearfix"></div>
    {elseif $input.type == 'meta_pages'}
      <div id="type_meta_pages" class="panel col-lg-7">
        <table class="table">
          <thead>
            <tr>
              <th>
                <input type="checkbox"
                name="checkme_meta_pages"
                id="checkme-meta-pages"
                class="noborder"
                onclick="checkDelBoxes(this.form, '{$input.name|escape:'htmlall':'UTF-8'}', this.checked)" />
              </th>
              <th>{l s='ID' mod='privateshop'}</th>
              <th>{l s='Page' mod='privateshop'}</th>
              <th>{l s='Title' mod='privateshop'}</th>
            </tr>
          </thead>
          <tbody>
            {foreach $input.values as $key => $page}
              <tr {if $key%2}class="alt_row"{/if}>
                <td>
                  <input type="checkbox"
                  class="cmsPageBox"
                  name="meta_pages[]"
                  id="meta_page_{$page.id_meta|escape:'htmlall':'UTF-8'}"
                  value="{$page.id_meta|escape:'htmlall':'UTF-8'}"
                  {if isset($fields_value.meta_pages) AND in_array($page.id_meta, $fields_value.meta_pages)}checked="checked"{/if} />
                </td>
                <td>
                  {$page.id_meta|escape:'htmlall':'UTF-8'}
                </td>
                <td>
                  {$page.page|escape:'htmlall':'UTF-8'}
                </td>
                <td>
                  <label class="control-label" for="meta_page_{$page.id_meta|escape:'htmlall':'UTF-8'}">
                    {$page.title|escape:'htmlall':'UTF-8'}
                  </label>
                </td>
              </tr>
            {/foreach}
          </tbody>
        </table>
      </div>
      <div class="clearfix"></div>
    {elseif $input.type == "product"}
      <div id="type_product" class="col-lg-9">
        <input type="hidden" name="inputPrivateProducts" id="inputPrivateProducts" value="{if isset($products) AND $products}{foreach from=$products item=product}{$product.id_product|escape:'htmlall':'UTF-8'}-{/foreach}{/if}" />
        <input type="hidden" name="namePrivateProducts" id="namePrivateProducts" value="{if isset($products) AND $products}{foreach from=$products item=product}{$product.name|escape:'html':'UTF-8'}¤{/foreach}{/if}" />
        <div id="ajax_choose_product">
          <div class="input-group">
            <input type="text" id="private_product_input" name="private_product_input" />
            <span class="input-group-addon"><i class="icon-search"></i></span>
          </div>
        </div>
        <p class="help-block">{l s='Search and add product(s) to restriction list.' mod='privateshop'}</p>
        <br>

        <div id="divPrivateProducts">
          {if isset($products) AND $products}
            {foreach from=$products item=product}
              <div class="form-control-static panel">
                <button type="button" class="btn btn-danger removePrivateProducts" name="{$product.id_product|escape:'htmlall':'UTF-8'}">
                  <i class="icon-trash text-danger"></i>
                </button>
                <input type="hidden" name="product[]" value="{$product.id_product|escape:'htmlall':'UTF-8'}">
                {$product.name|escape:'html':'UTF-8'}{if !empty($product.reference)}&nbsp;{l s='(ref: %s)' sprintf=$product.reference mod='privateshop'}{/if}
              </div>
            {/foreach}
          {/if}
        </div>
      </div>
      <div class="clearfix"></div>
    {elseif $input.type == 'pwd'}
      <div class="col-lg-12">
          <div class="col-lg-8">
            <input class="form-control" id="code" type="text" name="passwd" value="{if isset($fields_value.passwd) AND $fields_value.passwd}{$fields_value.passwd|escape:'htmlall':'UTF-8'}{/if}">
          </div>
          <div class="col-lg-4">
            <a class="btn btn-info" href="javascript:genPwd(16);">
                <i class="icon-random"></i>
                {l s='Generate password' mod='privateshop'}
            </a>
          </div>
      </div>
    {else}
      {$smarty.block.parent}
    {/if}
{/block}

{block name="script"}

  var pages = "{if isset($fields_value) AND isset($fields_value.type)}{$fields_value.type|escape:'htmlall':'UTF-8'}{/if}";
  if (typeof pages === 'undefined' || pages === '') {
    pages = $('#type option:selected').val();
  }

  filterPages(pages);

  $(document).on('change', '#type', function(){
    filterPages($(this).val());
  });

  $('#divPrivateProducts').delegate('.removePrivateProducts', 'click', function(){
    self.removePrivateProducts($(this).attr('name'));
  });

  var options = {
    minChars: 1,
    autoFill: true,
    max:20,
    matchContains: true,
    mustMatch:false,
    scroll:false,
    cacheLength:0,
    formatItem: function(item) {
      var itemStringToReturn = item[item.length - 1];
      for(var istr = 0; istr < item.length - 1;istr++) {
        itemStringToReturn += " " + item[istr];
      }
      return itemStringToReturn;
    },

  };

  $('#private_product_input')
  .autocomplete("{$context_link->getAdminLink('AdminPrivatePages') nofilter}&exclude_packs=0&excludeVirtuals=0",options)
  .result(addProduct);
  $('#private_product_input').setOptions({
    extraParams: {
      ajax: true,
      action: 'fetchProducts',
      excludeIds: getPrivateProduct()
    }
  });

  function addProduct(event, data, formatted)
  {
    if (data !== null) {
      var productId = data[1];
      var productName = data[0];

      var $divPrivateProducts = $('#divPrivateProducts');
      var $inputPrivateProducts = $('#inputPrivateProducts');
      var $namePrivateProducts = $('#namePrivateProducts');

      /* delete product from select + add product line to the div, input_name, input_ids elements */
      $divPrivateProducts.html($divPrivateProducts.html() + '<div class="form-control-static panel"><button type="button" class="removePrivateProducts btn btn-danger" name="' + productId + '"><i class="icon-trash text-danger"></i></button>&nbsp;'+ productName +'</div><input type="hidden" name="product[]" value="' + productId + '">');
      $namePrivateProducts.val($namePrivateProducts.val() + productName + '¤');
      ($inputPrivateProducts.val() !== 'undefined')?$inputPrivateProducts.val($inputPrivateProducts.val() + productId + '-'):$inputPrivateProducts.val(productId + '-');
      $('#private_product_input').val('');
      $('#private_product_input').setOptions({
        extraParams: {
          ajax: true,
          action: 'fetchProducts',
          excludeIds : getPrivateProduct()
        }
      });
    }
  }

  function getPrivateProduct() {
    if ($('#inputPrivateProducts').val() !== undefined) {
      return $('#inputPrivateProducts').val().replace(/\-/g,',');
    }
  }

  function removePrivateProducts(id)
  {
    var div = getE('divPrivateProducts');
    var input = getE('inputPrivateProducts');
    var name = getE('namePrivateProducts');

    // Cut hidden fields in array
    var inputCut = input.value.split('-');
    var nameCut = name.value.split('¤');

    if (inputCut.length != nameCut.length) {
      return jAlert('Bad size');
    }

    // Reset all hidden fields
    input.value = '';
    name.value = '';
    div.innerHTML = '';
    for (i in inputCut)
    {
      // If empty, error, next
      if (!inputCut[i] || !nameCut[i]) {
        continue ;
      }

      // Add to hidden fields no selected products OR add to select field selected product
      if (inputCut[i] != id) {
        input.value += inputCut[i] + '-';
        name.value += nameCut[i] + '¤';
        div.innerHTML += '<div class="form-control-static panel"><button type="button" class="removePrivateProducts btn btn-danger" name="' + inputCut[i] +'"><i class="icon-trash text-danger"></i></button>&nbsp;' + nameCut[i] + '</div><input type="hidden" name="product[]" value="' + inputCut[i] +'">';
      } else {
        $('#selectRestrictedProducts').append('<option selected="selected" value="' + inputCut[i] + '-' + nameCut[i] + '">' + inputCut[i] + ' - ' + nameCut[i] + '</option>');
      }
    }

    $('#private_product_input').setOptions({
      extraParams: {
        ajax: true,
        action: 'fetchProducts',
        excludeIds : getPrivateProduct()
      }
    });
  };

  function filterPages(pages) {
    var filtered_types = filter_types(pages);
    if (filtered_types.length) {
      $('#type_' + pages).closest('.form-group').show();
      for (var i = 0; i < filtered_types.length; i++) {
        $('#type_' + filtered_types[i]).closest('.form-group').hide();
      }
    }
  }

  function filter_types(type) {
    var pre_types = ["product", "category", "cms", "meta_pages"];
    if (typeof type !== 'undefined') {
      var index = pre_types.indexOf(type);
      if (index > -1) {
        pre_types.splice(index, 1);
      }
    }
    return pre_types;
  }

  /* Code generator for Affiliation and vouchers */
  function genPwd(size) {
    getE('code').value = '';
    /* There are no O/0 in the codes in order to avoid confusion */
    var chars = "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*()_=+-";
    for (var i = 1; i <= size; ++i)
      getE('code').value += chars.charAt(Math.floor(Math.random() * chars.length));
  }

  function getE(name) {
    if (document.getElementById)
      var elem = document.getElementById(name);
    else if (document.all)
      var elem = document.all[name];
    else if (document.layers)
      var elem = document.layers[name];
    return elem;
  }
{/block}

