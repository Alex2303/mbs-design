{*
* PrivateShop
*
* Do not edit or add to this file.
* You are not authorized to modify, copy or redistribute this file.
* Permissions are reserved by FME Modules.
*
*  @author    FME Modules
*  @copyright 2020 FME Modules All right reserved
*  @license   FME Modules
*  @category  FMM Modules
*  @package   PrivateShop
*}

{include file="$tpl_dir./errors.tpl"}
<div class="box">
    {if isset($message) AND $message}
      {$message nofilter} {* HTML Content *}
      <hr>
    {else}
      <p class="alert alert-warning warning">
        {l s='This page is passowrd protected. Please enter password to access the page content.' mod='privateshop'}
      </p>
      <br>
    {/if}
    <div class="protected-form-wrapper" style="margin: 0 auto; width: 70%;">
      <form id="private-page" action="{$link->getModuleLink('privateshop', 'protected', [], true)|escape:'htmlall':'UTF-8'}" method="post">
        <div id="protect-access-error"></div>
          <section>
            <div class="form-group row ">
                <label class="col-md-3 form-control-label required">
                  {l s='Password' mod='privateshop'}
                </label>
                <div class="col-md-6">
                    <input id="password-field" class="form-control" name="password" required="required" type="password">
                </div>
                <div class="col-md-2">
                    <button id="submit-private" class="btn btn-primary" type="submit">{l s='Access Content' mod='privateshop'}</button>
                </div>
                <input name="submitLogin" value="1" type="hidden">
            </div>
          </section>
      </form>
    </div>
</div>