{*
* PrivateShop
*
* Do not edit or add to this file.
* You are not authorized to modify, copy or redistribute this file.
* Permissions are reserved by FME Modules.
*
*  @author    FME Modules
*  @copyright 2020 FME Modules All right reserved
*  @license   FME Modules
*  @category  FMM Modules
*  @package   PrivateShop
*}

<!-- inline css -->
<style type="text/css">
{if isset($field_values) AND $field_values.bg_type}
{if $field_values.bg_type == "background-image" AND isset($field_values.bg_img)}

{* #bg-private-image *}
{literal}
#module-privateshop-private #wrapper, #module-privateshop-private div.columns-container {
    background:url("{/literal}{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/img/private/{$field_values.bg_img|escape:'htmlall':'UTF-8'}{literal}") no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    width: 100%;

}
{/literal}
{elseif $field_values.bg_type == "background-video" AND isset($field_values.bg_video_img)}
{* #bg-private-image_video *}
{literal}
#module-privateshop-private #wrapper,  #module-privateshop-private div.columns-container {
    background:url("{/literal}{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/img/private/tmp/{$field_values.bg_video_img|escape:'htmlall':'UTF-8'}{literal}") no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    width: 100%;
}
{/literal}
{if $version >= '1.7'}
{literal}
#module-privateshop-private #header,  #module-privateshop-private #footer {
    position: relative;
    z-index: 99;
    background: #333;
}
{/literal}
{/if}
{else if isset($field_values.bg_color) AND $field_values.bg_type == 'background-color'}
{* div#bg-private-color *}
{literal}
#module-privateshop-private #wrapper, #module-privateshop-private div.columns-container {
    height: 100%;
    margin: 0;
    width: 100%;
    background: {/literal}{$field_values.bg_color|escape:'htmlall':'UTF-8'}!important;{literal};
}
{/literal}
{/if}
{/if}
{literal}
.bg_opacity {
    background:rgb(241,241,241,{/literal}{$field_values.bg_opacity|escape:'htmlall':'UTF-8'}{literal}) !important;
}
.bg_opacity_white {
    background:rgb(255,255,255,{/literal}{$field_values.bg_opacity/2|escape:'htmlall':'UTF-8'}{literal}) !important;
}
#gif_loader {
    background-color: rgba(255,255,255,0.5);
    position: absolute; left: 0; top: 0; width: 100%; height: 101%; z-index: 9;
    background-image:url("{/literal}{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/img/load.gif"{literal});
    background-repeat: no-repeat;
    background-position: center;
}
#new-private-account { position: relative}
.date-select > label { margin-left: 15px!important;}
.show_comment { display: inline-block!important; }
</style><!--/inline css-->
{/literal}
<script type="text/javascript">
//<![CDATA[
    var baseUri = "{$base_uri|escape:'htmlall':'UTF-8'}";
    var token = "{$token|escape:'htmlall':'UTF-8'}";
    var ajax_url = "{$ajax_link|escape:'htmlall':'UTF-8'}";
    var psNew = parseInt("{if $version ge '1.7'}1{else}0{/if}");
//]]>
</script>
{if isset($field_values) AND $field_values.bg_type}
    {if $field_values.bg_type == "background-video" AND isset($field_values.bg_video)}
        <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function() {
            setBGvideo('{$field_values.bg_video}');
        });
        </script>
    {/if}
{/if}
{if $persist == 1 AND isset($persist)}
    <script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
        forgot_password();
    });
    </script>
{/if}
<div {if $field_values.priv_form_theme == 'mod'}class="{if $version >= 1.7}ps17{else}ps16{/if} private_modern_theme"{/if}>
    <div id="private-wrapper"
        {if isset($field_values.position) AND $field_values.position AND $field_values.position == 'left'}class="bg_opacity"{elseif $field_values.position == 'right'} class="bg_opacity"{/if}
        style="{if isset($field_values.position) AND $field_values.position AND $field_values.position == 'left'}float: left; margin-left:3%;{elseif $field_values.position == 'right'}float: right; margin-right:3%;{elseif $field_values.position == 'center'}margin:0 auto;{/if}"
        {if isset($field_values) AND $field_values.position == 'center'}class="center_align bg_opacity"{/if}>
        <div id="privatebox">
            <div id="fmm_{if $version >= 1.7}ps17{else}ps16{/if}" class="container bg_opacity_white">
                <p id="logo_basic">
                    <img src="{if isset($field_values.custom_logo) && $field_values.custom_logo > 0}{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/img/private/tmp/{$field_values.custom_logo_img|escape:'htmlall':'UTF-8'}{else}{$logo_url|escape:'htmlall':'UTF-8'}{/if}" alt="logo" />
                </p>
                {if $deadend}
                    {if isset($field_values.restrict_message) AND empty($field_values.restrict_message)}
                        <div id="restricted">
                            <h1>{l s='You do not have permission to view this page.' mod='privateshop'}</h1>
                        </div>
                    {else}
                        {$field_values.restrict_message nofilter}{*HTML Content*}
                    {/if}
                {else}
                    {if $field_values.show_store_title > 0}<h1 class="pshop_title_shop">{$shop_name|escape:'htmlall':'UTF-8'}</h1>{/if}
                    <!-- <h2>{l s='Private Login' mod='privateshop'}</h2> -->
                    <div id="center_column" class="private_login">
                        {if $version >= 1.7}
                            {include file="module:privateshop/views/templates/front/errors.tpl"}
                        {else}
                            {include file="$tpl_dir./errors.tpl"}
                        {/if}
                        <div id="error_holder"></div>
                        {if isset($field_values) AND isset($field_values.active_signup) AND $field_values.active_signup == 1}
                        <!-- create form -->
                        <div id="new-private-account" <!--style="display:none;-->">
                            <form method="post" class="js-customer-form" id="customer-form" action="">
                                <section>
                                <input type="hidden" value="" name="id_customer">
                                {if isset($field_values.gender_opt) && $field_values.gender_opt <= 0}
                                <div class="form-group row social_title">
                                    <label class="col-md-3 form-control-label">
                                    {l s='Social title' mod='privateshop'}
                                    </label>
                                    <div class="col-md-6 form-control-valign">
                                        <label class="radio-inline">
                                        <span class="custom-radio">
                                        <input type="radio" value="1" name="id_gender">
                                        <span></span>
                                        </span>
                                        {l s='Mr.' mod='privateshop'}
                                        </label>
                                        <label class="radio-inline">
                                        <span class="custom-radio">
                                        <input type="radio" value="2" name="id_gender">
                                        <span></span>
                                        </span>
                                        {l s='Mrs.' mod='privateshop'}
                                        </label>
                                    </div>
                                    <div class="col-md-3 form-control-comment">
                                    </div>
                                </div>
                                {/if}
                                <div class="form-group row ">
                                    <label class="col-md-3 form-control-label required">
                                    {l s='First name' mod='privateshop'}
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" placeholder="{l s='First name' mod='privateshop'}" required="" value="" name="firstname" class="form-control">
                                    </div>
                                    <div class="col-md-3 form-control-comment">
                                    </div>
                                </div>
                                <div class="form-group row ">
                                    <label class="col-md-3 form-control-label required">
                                    {l s='Last name' mod='privateshop'}
                                    </label>
                                    <div class="col-md-6">
                                        <input type="text" required="" placeholder="{l s='Last name' mod='privateshop'}" value="" name="lastname" class="form-control">
                                    </div>
                                    <div class="col-md-3 form-control-comment">
                                    </div>
                                </div>
                                <div class="form-group row ">
                                    <label class="col-md-3 form-control-label required">
                                    {l s='Email' mod='privateshop'}
                                    </label>
                                    <div class="col-md-6">
                                        <input type="email" required="" placeholder="{l s='Email' mod='privateshop'}" value="" name="email_account" class="form-control">
                                    </div>
                                    <div class="col-md-3 form-control-comment">
                                    </div>
                                </div>
                                <div class="form-group row ">
                                    <label class="col-md-3 form-control-label required">
                                    {l s='Password' mod='privateshop'}
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-group js-parent-focus">
                                            <input type="password" required="" value="" placeholder="{l s='Password' mod='privateshop'}" name="password" class="form-control js-child-focus js-visible-password">
                                            <span class="input-group-btn pshop_show_hide">
                                            <button data-text-hide="{l s='Hide' mod='privateshop'}" onclick="toggleVis(this);" data-text-show="{l s='Show' mod='privateshop'}" data-action="show-password" type="button" class="btn">
                                            {l s='Show' mod='privateshop'}
                                            </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-control-comment">
                                    </div>
                                </div>
                                {if isset($field_values.bday) && $field_values.bday <= 0}
                                <div class="form-group row ">
                                    <div class="birth-date-select">
                                        <label class="col-lg-3">{l s='Birthday' mod='privateshop'}</label>
                                        <div class="col-lg-6">
                                            <div>
                                                <select id="days" name="days" class="form-control no-uniform">
                                                    <option value="">-</option>
                                                    {foreach from=$days item=day}
                                                        <option value="{$day}" {if ($sl_day == $day)} selected="selected"{/if}>{$day}&nbsp;&nbsp;</option>
                                                    {/foreach}
                                                </select>
                                            </div><br>
                                            <div>
                                                <select id="months" name="months" class="form-control no-uniform">
                                                    <option value="">-</option>
                                                    {foreach from=$months key=k item=month}
                                                        <option value="{$k}" {if ($sl_month == $k)} selected="selected"{/if}>{l s=$month mod='privateshop' }&nbsp;</option>
                                                    {/foreach}
                                                </select>
                                            </div><br>
                                            <div>
                                                <select id="years" name="years" class="form-control no-uniform">
                                                    <option value="">-</option>
                                                    {foreach from=$years item=year}
                                                        <option value="{$year}" {if ($sl_year == $year)} selected="selected"{/if}>{$year}&nbsp;&nbsp;</option>
                                                    {/foreach}
                                                </select>
                                            </div><br>
                                            <div class="col-md-3 form-control-comment show_comment">
                                                {l s='Optional' mod='privateshop'}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/if}
                                {if isset($field_values.offers_opt) && $field_values.offers_opt <= 0}
                                <div class="form-group row ">
                                    <label class="col-md-3 form-control-label"></label>
                                    <div class="col-md-6">
                                        <span class="custom-checkbox">
                                        <input id="optin" class="no-uniform" type="checkbox" value="1" name="optin">
                                        {if $version >= 1.7}<span><i class="material-icons checkbox-checked"></i></span>{/if}
                                        <label for="optin">{l s='Receive offers from our partners' mod='privateshop'}</label>
                                        </span>
                                    </div>
                                    <div class="col-md-3 form-control-comment">
                                    </div>
                                </div>
                                {/if}
                                {if isset($field_values.nletter_opt) && $field_values.nletter_opt <= 0}
                                <div class="form-group row ">
                                    <label class="col-md-3 form-control-label"></label>
                                    <div class="col-md-6">
                                        <span class="custom-checkbox">
                                        <input id="newsletter" class="no-uniform" type="checkbox" value="1" name="newsletter">
                                        {if $version >= 1.7}<span><i class="material-icons checkbox-checked"></i></span>{/if}
                                        <label for="newsletter">{l s='Sign up for our newsletter' mod='privateshop'}<br><em></em></label>
                                        </span>
                                    </div>
                                    <div class="col-md-3 form-control-comment">
                                    </div>
                                </div>
                                {/if}
                                {$hook_create_account_form nofilter}
                                </section>
                                <footer class="form-footer clearfix">
                                <input type="hidden" value="1" name="submitCreate">
                                <button type="button" data-link-action="save-customer" onclick="registerNewUser(this);" class="btn btn-primary form-control-submit pull-xs-right">
                                {l s='Save' mod='privateshop'}
                                </button>
                                    <a href="javascript:void(0);" onclick="Login();">
                                        <span>{l s='Log in instead!' mod='privateshop'}</span>
                                    </a>
                                </footer>
                            </form>
                            <div class="clearfix"></div>
                            <div id="gif_loader" style="display: none"></div>
                        </div> <!-- /create form ends-->
                        {/if}
                            <!-- login form -->
                        <div id="private-login">
                            <form action="{$link->getPageLink('authentication', true)|escape:'htmlall':'UTF-8'}" method="post" id="login_form" class="box">
                                <h2 class="private-subheading">{if isset($field_values) AND isset($field_values.login_title) AND $field_values.login_title}{$field_values.login_title|escape:'htmlall':'UTF-8'}{else}{l s='Private Login' mod='privateshop'}{/if}</h2>
                                <div class="form_content clearfix">
                                <table class="private_login_table">
                                    <tr class="pshop_fields_row">
                                        <div class="form-group">
                                            <td><label for="email">{l s='Email address' mod='privateshop'}</label></td>
                                            <td colspan="3"><input placeholder="{l s='Email address' mod='privateshop'}" class="is_required validate account_input form-control" data-validate="isEmail" type="text" id="email" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email|stripslashes|escape:'htmlall':'UTF-8'}{/if}" /></td>
                                        </div>
                                    </tr>
                                    <tr class="exttra_row"><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                    <tr class="pshop_fields_row">
                                        <div class="form-group">
                                            <td><label for="passwd">{l s='Password' mod='privateshop'}</label></td>
                                            <td colspan="3"><input placeholder="{l s='Password' mod='privateshop'}" class="is_required validate account_input form-control" type="password" data-validate="isPasswd" id="passwd" name="passwd" value="{if isset($smarty.post.passwd)}{$smarty.post.passwd|stripslashes|escape:'htmlall':'UTF-8'}{/if}" /></td>
                                        </div>
                                    </tr>
                                    <tr class="exttra_row"><td>&nbsp;</td><td>&nbsp;</td></tr>
                                </table>
                                <ul id="pshop_bottom_footer">
                                        <li><p class="lost_password form-group"><a id="lost-password" href="javascript:;" title="{l s='Recover your forgotten password' mod='privateshop'}" rel="nofollow" onclick="forgot_password()">{l s='Forgot your password?' mod='privateshop'}</a></p></li>
                                        <li class="submit">
                                            {if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'htmlall':'UTF-8'}" />{/if}
                                        <div>
                                            {if $version >= 1.6}
                                            &nbsp;<button type="submit" id="SubmitLogin" name="SubmitLogin" class="button btn btn-default button-medium">
                                                <span>
                                                    <i class="icon-lock left"></i>
                                                    {l s='Sign in' mod='privateshop'}
                                                </span>
                                            </button>
                                            {else}
                                                <input type="submit" id="SubmitLogin" name="SubmitLogin" class="button" value="{l s='Log in' mod='privateshop'}"/>
                                            {/if}
                                        </div>
                                        <div>
                                            {if isset($field_values) AND isset($field_values.active_signup) AND $field_values.active_signup == 1}
                                            &nbsp;<a class="button btn btn-default button-medium exclusive" type="submit" id="register" name="register" onclick="SignUp();">
                                                <span>
                                                    {l s='Sign Up' mod='privateshop'}
                                                </span>
                                            </a>
                                            {/if}
                                        </div>
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div> <!-- /login form ends -->
                        <div id="private-lost-password" style="display:none;">
                            {if $version >= 1.7}
                                {include file="module:privateshop/views/templates/front/password_17.tpl"}
                            {else}
                                {include file="./password.tpl"}
                            {/if}
                        </div>
                    </div>
                    <div id="private-back" class="col-lg-12" style="display:none;">
                        <p class="alert alert-info info">{l s='Your account is under review. Your will receive confirmation email soon.' mod='privateshop'}</p>
                        <button onclick="window.location.reload();" class="btn btn-default col-lg-12">{l s='Back' mod='privateshop'}</button>
                    </div>
                {/if}
            </div>
        </div>
        <div class="clearfix"></div>
    </div><div class="clearfix"></div>
</div>