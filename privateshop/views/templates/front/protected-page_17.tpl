{*
 * PrivateShop
 *
 * Do not edit or add to this file.
 * You are not authorized to modify, copy or redistribute this file.
 * Permissions are reserved by FME Modules.
 *
 *  @author    FME Modules
 *  @copyright 2020 FME Modules All right reserved
 *  @license   FME Modules
 *  @category  FMM Modules
 *  @package   PrivateShop
 *}

{extends file='page.tpl'}

{block name='page_content'}
    <div class="">
    {if isset($message) AND $message}
      {$message nofilter} {* HTML Content *}
      <hr>
    {else}
      <p class="alert alert-warning warning">
        {l s='This page is passowrd protected. Please enter password to access the page content.' mod='privateshop'}
      </p>
    {/if}
      <form id="private-page" action="{$link->getModuleLink('privateshop', 'protected', [], true)|escape:'htmlall':'UTF-8'}" method="post">
        <div id="protect-access-error"></div>
          <section>
            <div class="form-group row ">
                <label class="col-md-3 form-control-label required">
                  {l s='Password' mod='privateshop'}
                </label>
                <div class="col-md-6">
                    <div class="input-group js-parent-focus">
                        <input class="form-control js-child-focus js-visible-password" name="password" required="required" type="password">
                        <span class="input-group-btn">
                          <button class="btn" type="button" data-action="show-password" data-text-show="{l s='Show' mod='privateshop'}" data-text-hide="{l s='Hide' mod='privateshop'}">{l s='Show' mod='privateshop'}
                          </button>
                        </span>
                    </div>
                </div>
                <div class="col-md-3 form-control-comment">
                </div>
            </div>
          </section>
          <footer class="form-footer text-sm-center clearfix">
            <input name="submitLogin" value="1" type="hidden">
            <button id="submit-private" class="btn btn-primary" type="submit">
              {l s='Access Content' mod='privateshop'}
            </button>
        </footer>
      </form>
    </div>
{/block}