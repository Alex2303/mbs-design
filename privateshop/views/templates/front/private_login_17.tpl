{*
* PrivateShop
*
* Do not edit or add to this file.
* You are not authorized to modify, copy or redistribute this file.
* Permissions are reserved by FME Modules.
*
*  @author    FME Modules
*  @copyright 2020 FME Modules All right reserved
*  @license   FME Modules
*  @category  FMM Modules
*  @package   PrivateShop
*}
{extends file=$layout}


{block name='content'}
	{if isset($js_files) && $js_files}
		{foreach from=$js_files item=js_uri}
			<script type="text/javascript" src="{$js_uri|escape:'htmlall':'UTF-8'}"></script>
		{/foreach}
	{/if}
	<script type="text/javascript" src="{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/js/validate.js"></script>
	{if $version >= 1.6}
		<script type="text/javascript" src="{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/js/authentication16.js"></script>
		<link rel="stylesheet" href="{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/css/authentication16.css" type="text/css" media="all" charset="utf-8" />
	{else}
		<script type="text/javascript" src="{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/js/authentication15.js"></script>
		<link rel="stylesheet" href="{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/css/authentication15.css" type="text/css" media="all" charset="utf-8" />
	{/if}
	{if isset($field_values) AND $field_values.bg_type}
		{if $field_values.bg_type == "background-video" AND isset($field_values.bg_video)}
			<script type="text/javascript" src="{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/js/jquery.tubular.1.0.js"></script>
		{/if}
	{/if}
	<script type="text/javascript" src="{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/js/authentication16.js"></script>

	<link rel="shortcut icon" href="{$favicon_url|escape:'htmlall':'UTF-8'}" />
	<link rel="stylesheet" href="{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/css/private.css" type="text/css" charset="utf-8" />
	<link rel="stylesheet" href="{$css_dir|escape:'htmlall':'UTF-8'}theme.css" type="text/css" media="all" charset="utf-8" />
	<!--police Karla-->
	<link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
	<!--fontawesome-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">

{literal}
	<!-- inline css -->
	<style type="text/css">

		#bg-private-image{
			background:url("{/literal}{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/img/private/{$field_values.bg_img|escape:'htmlall':'UTF-8'}{literal}") no-repeat center center fixed;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			width: 100%;
		}

		#bg-private-image_video{
			background:url("{/literal}{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/img/private/tmp/{$field_values.bg_video_img|escape:'htmlall':'UTF-8'}{literal}") no-repeat center center fixed;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			width: 100%;
		}

		#gif_loader { background-color: rgba(255,255,255,0.5);
			position: absolute; left: 0; top: 0; width: 100%; height: 101%; z-index: 9;
			background-image:url("{/literal}{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/img/load.gif"{literal});
			background-repeat: no-repeat;
			background-position: center;
		}
		#new-private-account { position: relative}
		.date-select > label { margin-left: 15px!important;}
		.show_comment { display: inline-block!important; }
	</style><!--/inline css-->
{/literal}
	<figure class="privatebox_cover">
		<div id="container_animation">
			<p class="text_cover">Les précommandes MBS Design</p>
			<br>
			<div id="text_animation"></div>
			<div id="cursor"></div>
		</div>
	</figure>
	<div class="grid__container grid__container_one">
		<figure class="background_img background_img1"></figure>
		<div class="grid_content">
			<h1 class="grid__title grid__title_one bold">Pas de stock, pas de gâchis !</h1>
			<p class="grid__description grid__description_one mt-2">	
				L’industrie de l’ameublement produit chaque année une quantité de meubles et objets de décoration invendus qui finiront dans une décharge. Surproduction, invendus, mauvaise qualité et gaspillage des ressources sont un enjeu majeur pour l’environnement. 
				<br>
				<br>
				Pourtant, nous avons ensemble - marques et consommateurs - la possibilité de changer la donne en adoptant une nouvelle manière de produire et d’acheter plus responsable et plus durable. Comment ? En adoptant le principe de la précommande
			</p>
		</div>

	</div>

	<div class="grid__container grid__container_two">
		<div class="grid_content">
			<h1 class="grid__title grid__title_two bold">Un prix juste et honnête</h1>
			<p class="grid__description grid__description_two mt-2">	
				Chaque semaine, MBS Design vous proposera un choix limité de meubles et luminaires finement sélectionnés et négociés en amont avec nos fournisseurs.
				<br>
				<br>
				Les invendus, le gaspillage des matières premières ainsi que le stockage impactent de manière négative le coût de production d’un produit et par conséquent son prix final.
				<br>
				<br>
				Le système de précommande nous permet de réduire nos coûts de fonctionnement, notre marge et par conséquent de vous proposer des pièces éthiques et de qualité à un prix juste et raisonnable toute l’année. 
			</p>
		</div>
		<figure class="background_img background_img2"></figure>
	</div>

	<div class="grid__container grid__container_three">
		<figure class="background_img background_img3"></figure>
		<div class="grid_content">
			<h1 class="grid__title grid__title_three bold">Pourquoi cela met-il plus de temps ?</h1>
			<p class="grid__description grid__description_three mt-2">	
				« Parce que les plus belles choses se font toujours attendre »
				<br>
				<br>
				Le principe de la précommande impose que nous attendons la fin la vente éphémère pour lancer la production auprès de nos fournisseurs et partenaires.
				<br>
				<br>
				Les délais de fabrication et de livraison peuvent varier de 3 à 8 semaines en fonction des produits pour une livraison en Europe. Nous indiquerons le délai précis lors de chaque vente.
				<br>
				<br>
				Loin de l'immédiateté, nous croyons en un processus d'achat raisonné, en un design plus responsable, plus lent, respectueux de l’environnement et de l’humain.
				<br>
				<br>
				Consommer moins mais mieux en choisissant des meubles au prix juste et de qualité, faits pour durer et être aimées durant des années. Voilà la contrepartie de votre patience !
			</p>
			<button class="btn btn-default button button-medium exclusive btn_join mt-2" type="button" href="#new-private-account">Rejoignez nos membres</button>
		</div>

	</div>

	<div id="wrapper" 
		{if isset($field_values.position) AND $field_values.position AND $field_values.position == 'left'} 
			class="bg_opacity"
			{elseif $field_values.position == 'right'} 
				class="bg_opacity"
		{/if} 
		style="
		{if isset($field_values.position) AND $field_values.position AND $field_values.position == 'left'}
			float: left; margin-left:3%;
		{elseif $field_values.position == 'right'}
			float: right; margin-right:3%;
		{elseif $field_values.position == 'center'}
			margin:0 auto;
		{/if}" 
		{if isset($field_values) AND $field_values.position == 'center'}
			class="center_align bg_opacity"
		{/if}>

		<div id="privatebox" class="grid__container grid__container_four">
				
			{* <h2>{l s='Private Login' mod='privateshop'}</h2> *}
			
			{* <p id="logo_basic" class="logo"><img src="{if isset($field_values.custom_logo) && $field_values.custom_logo > 0}{$modules_dir|escape:'htmlall':'UTF-8'}privateshop/views/img/private/tmp/{$field_values.custom_logo_img|escape:'htmlall':'UTF-8'}{else}{$logo_url|escape:'htmlall':'UTF-8'}{/if}" alt="logo" /></p>{if $field_values.show_store_title > 0}
			<h1 class="pshop_title_shop">{l s='Private Login' mod='privateshop'}</h1>{/if} *}
			
			{* {include file="module:privateshop/views/templates/front/errors.tpl"}
			<div id="error_holder"></div> *}

			<div class="grid_content">
				{if isset($field_values) AND isset($field_values.active_signup) AND $field_values.active_signup == 1}
					<!-- create form -->
					<div id="new-private-account" style="display:none;">
						<h3 class="privatebox_description">Inscrivez-vous</h3>
						<form method="post" class="js-customer-form" id="customer-form">
								<input type="hidden" value="" name="id_customer">
								{if isset($field_values.gender_opt) && $field_values.gender_opt <= 0}
									<div class="form-group social_title">
										<label class="form-control-label">
											{l s='Social title' mod='privateshop'}
										</label>
										<div class="form-control-valign flex">
											<label class="radio-inline">
												<span class="custom-radio">
													<input type="radio" class="isEmtpy" value="1" name="id_gender" required>
														<span></span>
												</span>
												{l s='Mr.' mod='privateshop'}
											</label>
											<label class="radio-inline">
												<span class="custom-radio">
													<input type="radio" class="isEmtpy" value="2" name="id_gender" required>
													<span></span>
												</span>
												{l s='Mrs.' mod='privateshop'}
											</label>
										</div>

										{* <div class="col-md-12 form-control-comment">
										</div> *}

									</div>
								{/if}
								<div class="form-group">
									<label class="form-control-label required">
										{l s='First name' mod='privateshop'}
									</label>
										<input type="text" value="" name="firstname" class="form-control isEmpty" required>
										<span class="placeholder">{l s='First name' mod='privateshop'}</span>
									
									{* <div class="col-md-3 form-control-comment">
									</div> *}

								</div>
								<div class="form-group">
									<label class="form-control-label required">
										{l s='Last name' mod='privateshop'}
									</label>
										<input type="text" value="" name="lastname" class="form-control isEmpty" required>
										<span class="placeholder">{l s='Last name' mod='privateshop'}</span>
									
									{* <div class="col-md-3 form-control-comment">
									</div> *}

								</div>
								<div class="form-group">
									<label class="form-control-label required">
										{l s='Email' mod='privateshop'}
									</label>
										
									<input type="email" value="" name="email_account" class="form-control email isEmpty" required="">
									<span class="placeholder">{l s='Email' mod='privateshop'}</span>
									
									{* <div class="col-md-3 form-control-comment">
									</div> *}

								</div>
								<div class="form-group">
									<label class="form-control-label required">
										{l s='Password' mod='privateshop'}
									</label>
									<div class="input-group input-pwd js-parent-focus">
										<input type="password" value="" placeholder="**********" name="password" class="form-control js-child-focus js-visible-password isEmpty" required="">
										<span class="input-group-btn pshop_show_hide">
											<i class="far fa-eye showHide" onclick="toggleVis(this);" data-action="show-password"></i>
										</span>
									</div>

									{* <div class="col-md-3 form-control-comment">
									</div> *}

								</div>
								{if isset($field_values.bday) && $field_values.bday <= 0}
									<div class="form-group">
										<div class="birth-date-select">
											<label class="col-lg-3">{l s='Birthday' mod='privateshop'}</label>
											<div class="col-lg-6">
												<div>
													<select id="days" name="days" class="form-control">
														<option value="">-</option>
														{foreach from=$days item=day}
															<option value="{$day}" {if ($sl_day == $day)} selected="selected"{/if}>{$day}&nbsp;&nbsp;</option>
														{/foreach}
													</select>
												</div><br>
												<div>
													<select id="months" name="months" class="form-control">
														<option value="">-</option>
														{foreach from=$months key=k item=month}
															<option value="{$k}" {if ($sl_month == $k)} selected="selected"{/if}>{l s=$month mod='privateshop' }&nbsp;</option>
														{/foreach}
													</select>
												</div><br>
												<div>
													<select id="years" name="years" class="form-control">
														<option value="">-</option>
														{foreach from=$years item=year}
															<option value="{$year}" {if ($sl_year == $year)} selected="selected"{/if}>{$year}&nbsp;&nbsp;</option>
														{/foreach}
													</select>
												</div><br>

												{* <div class="col-md-3 form-control-comment show_comment">
													{l s='Optional' mod='privateshop'}
												</div> *}

											</div>
										</div>
									</div>
								{/if}
								{if isset($field_values.offers_opt) && $field_values.offers_opt <= 0}
									<div class="form-group">
										<label class="form-control-label">
										</label>
										<div>
											<span class="custom-checkbox">
												<input type="checkbox" value="1" name="optin">
												<span><i class="material-icons checkbox-checked"></i></span>
											<label>{l s='Receive offers from our partners' mod='privateshop'}</label>
											</span>
										</div>

										{* <div class="col-md-3 form-control-comment">
										</div> *}

									</div>
								{/if}
								{if isset($field_values.nletter_opt) && $field_values.nletter_opt <= 0}
									<div class="form-group">
										<label class="form-control-label">
										</label>
										<div class="p-0 col-md-5">
											<span class="custom-checkbox">
												<input type="checkbox" value="1" name="newsletter" class="newsletter-checkbox">
												<span><i class="material-icons checkbox-checked"></i></span>
												<label class="newsletter">{l s='Sign up for our newsletter' mod='privateshop'}<br><em></em></label>
											</span>
										</div>

										{* <div class="col-md-3 form-control-comment">
										</div> *}

									</div>
								{/if}
								{$hook_create_account_form nofilter}
							<div class="submit">
								<div>
									<input type="hidden" value="1" name="submitCreate">
									<button type="submit" data-link-action="save-customer" onclick="registerNewUser(this, event);" id="submitCreate" class="btn btn-primary form-control-submit pull-xs-right ">
										{l s='Save' mod='privateshop'}
									</button>
								</div>
								<div>
									<button type="submit" class="button btn btn-default button-medium">
										<a href="javascript:void(0);" onclick="Login();">
											<span class="white">{l s='Log in instead!' mod='privateshop'}</span>
										</a>
									</button>
								</div>
							</div>
						</form>
						
						<div class="clearfix"></div>
						<div id="gif_loader" style="display: none"></div>
						
					</div> <!-- /create form ends-->
				{/if}
				
				<!-- login form -->
				<div id="private-login">
					<h1 class="grid__title">Comment en profiter ?</h1>
					<p class="private-login_info">
						C’est très simple. La précommande fonctionne exactement comme pour un achat standard. 
						<br>
						<br>
						Vous n’avez qu’à vous inscrire ci-dessous pour accéder à l’espace des précommandes et découvrir chaque semaine la nouvelle sélection en exclusivité. 
					</p>
					<h3 class="privatebox_description">Connectez-vous</h3>
					<form action="{$link->getPageLink('authentication', true)|escape:'htmlall':'UTF-8'}" method="post" id="login_form" class="box">
						{* <h2 class="private-subheading">{if isset($field_values) AND isset($field_values.login_title) AND $field_values.login_title}{$field_values.login_title|escape:'htmlall':'UTF-8'}{else}{l s='Private Login' mod='privateshop'}{/if}</h2> *}
						<div class="clearfix">
							<div class="form-group">
								<label for="email">{l s='Email address' mod='privateshop'}</label>
								<input class="is_required validate account_input form-control email isEmpty" data-validate="isEmail" value="" onkeyup="this.setAttribute('value', this.value);" name="email" type="email" required />
								<span class="placeholder">{l s='Email address' mod='privateshop'}</span>
							</div>
							<div class="form-group">
								<label class="form-control-label required">
									{l s='Password' mod='privateshop'}
								</label>
								<div class="input-group input-pwd js-parent-focus">
									<label for="passwd">{l s='Password' mod='privateshop'}</label>
									<input type="password" value="" placeholder="**********" class="form-control js-child-focus js-visible-password isEmpty is_required validate account_input" data-validate="isPasswd" id="passwd" name="passwd" value="{if isset($smarty.post.passwd)}{$smarty.post.passwd|stripslashes|escape:'htmlall':'UTF-8'}{/if}" required="">
									<span class="input-group-btn pshop_show_hide">
										<i class="far fa-eye showHide" onclick="toggleVis(this);" data-action="show-password"></i>
									</span>
								</div>

								{* <div class="col-md-3 form-control-comment">
								</div> *}

							</div>
								<p class="lost_password form-group"><a id="lost-password" href="javascript:;" title="{l s='Recover your forgotten password' mod='privateshop'}" rel="nofollow" onclick="forgot_password()">{l s='Forgot your password?' mod='privateshop'}</a></p>
								<div class="submit">
									{if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'htmlall':'UTF-8'}" />{/if}
									<div>
										{if $version >= 1.6}
											<button type="submit" id="SubmitLogin" name="SubmitLogin" class="button btn btn-default button-medium">
												<span>
													<i class="icon-lock left"></i>
													{l s='Sign in' mod='privateshop'}
												</span>
											</button>
										{else}
											<input type="submit" id="SubmitLogin" name="SubmitLogin" class="button" value="{l s='Log in' mod='privateshop'}"/>
										{/if}
									</div>
									<div>
										{if isset($field_values) AND isset($field_values.active_signup) AND $field_values.active_signup == 1}
											<a type="submit" class="btn btn-default button button-medium exclusive" id="register" name="register" onclick="SignUp();">
												<span>
													{l s='Sign Up' mod='privateshop'}
												</span>
											</a>
										{/if}
									</div>
										
								</div>
						</div>
					</form>
					
				</div> <!-- /login form ends -->
				<div id="private-lost-password" style="display:none;">
				{include file="module:privateshop/views/templates/front/password_17.tpl"}</div>
			
				<div id="private-back" class="col-lg-12" style="display:none;">
					<p class="alert alert-info info">{l s='Your account is under review. Your will receive confirmation email soon.' mod='privateshop'}</p>
					<button onclick="window.location.reload();" class="btn btn-default col-lg-12">{l s='Back' mod='privateshop'}</button>
				</div>
			</div>

			<figure class="background_img background_img4"></figure>

		</div>

		<div class="clearfix"></div>
		{* <div class="col-md-5 wrap-img">
			<figure class="col-md-5 background-img"></figure>
		</div> *}

		<figure class="grid_img background_img3"></figure>

	</div>
	
	<div class="clearfix"></div>


	<script type="text/javascript">
		//<![CDATA[
		var baseUri = "{$base_uri|escape:'htmlall':'UTF-8'}";
		var token = "{$token|escape:'htmlall':'UTF-8'}";
		var ajax_url = "{$ajax_link|escape:'htmlall':'UTF-8'}";
		var psNew = parseInt("{if $version ge '1.7'}1{else}0{/if}");
		{literal}
		// load() event and resize() event are combined
		$(window).ready(responsiveFn).resize(responsiveFn);
		function SignUp()
		{
			$('.alert-danger, .error').hide();
			$('#private-login').hide();
			$('#new-private-account').show();
			$('.alert').addClass('private_error_resp');
		}

		function Login()
		{
			$('.alert-danger, .error').hide();
			$('#private-login').show();
			$('#new-private-account').hide();
			$('.alert').addClass('private_error_resp');
		}

		function forgot_password()
		{
			//$('#ps17_errors').html('').hide();
			$('#private-login').hide();
			$('#private-lost-password').show();
			$('.alert').addClass('private_error_resp');
		}

		function BackToLogin()
		{
			if (typeof baseUri === 'undefined') {
				baseUri = prestashop.urls.base_url;
			}

			if (getUrlParameter('reset_token') !== '') {
				window.location.replace(baseUri);
			} else {
				$('.private_error_resp').html('').hide();
				$('#private-lost-password').hide();
				$('#private-login').fadeIn('slow');
			}
		}

		function getUrlParameter(name)
		{
			name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
			var results = regex.exec(location.search);
			return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
		}

		function responsiveFn()
		{
			width = $( window ).width();
			height = $( window ).height();

			if(width <= 320) {
				$('#login_form').removeClass('box');
			}
			else {
				$('#login_form').addClass('box');
			}
		}
		function toggleVis(e) {
			var elm = $(e).closest('.input-group').children('input.js-visible-password');
			if (elm.attr('type') === 'password') {
				elm.attr('type', 'text');
				$(e).text($(e).data('textHide'));
				$('.fa-eye').toggleClass('fa-eye-slash');
			} else {
				elm.attr('type', 'password');
				$(e).text($(e).data('textShow'));
				$('.fa-eye').removeClass('fa-eye-slash');
			}

		};
		function registerNewUser(el, e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			var gif_loader = $('#gif_loader');
			gif_loader.show();
			var form_data = $('#new-private-account').serialize();
			var requestData = {
				type : "POST",
				cache : false,
				url : ajax_url,
				dataType : "json",
				data : form_data,
				success: function(jsonData) {
					if (jsonData.errors > 0) {
						var __html = '<div class="alert alert-danger" id="ps17_errors"><ol><li>' + jsonData.html + '</li></ol></div>';
						$('#error_holder').html(__html);
						gif_loader.hide();
					} else {
						$('#error_holder').html('');
						if (jsonData.redirect === true) {
							window.location = jsonData.redirect_url;
						} else {
							$('#new-private-account').html(jsonData.message);
						}

						$('#private-back').show();
						gif_loader.hide();
					}
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					gif_loader.hide();
				}
			};
			$.ajax(requestData);
		}

		$(document).on('click', '#SubmitLogin', function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			//var gif_loader = $('#gif_loader');
			//gif_loader.show();
			var jsonData = {
				type : "POST",
				cache : false,
				url : ajax_url,
				dataType : "json",
				data : {
					action : 'privateLogin',
					ajax : true,
					email : $.trim($('input[name=email]').val()),
					passwd : $.trim($('input[name=passwd]').val())
				},
				success: function(response) {
					if (response.errors > 0) {
						var __html = '<div class="alert alert-danger" id="ps17_errors"><ol><li>'+response.html+'</li></ol></div>';
						$('#error_holder').html(__html);
						//gif_loader.hide();
					} else if (response.success) {
						$('#error_holder').html('');
						//gif_loader.hide();
						window.location.reload();
					}
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					//gif_loader.hide();
				}
			};
			$.ajax(jsonData);
		});
		{/literal}
		//]]>
	

		/**** Code Alexandre ****/
		var msg = "";

		$(".email").on('invalid', function(e){
			if(e.target.validity.typeMismatch){
				msg = 'L\'adresse email n\'est pas valide !';
				e.target.value = ' ';
			}
			e.target.setCustomValidity(msg);
		});

		$(".isEmpty").on('invalid', function(e){
			if(e.target.validity.valueMissing){
				msg = 'Veuillez renseigner le champ !';
			}
		});
		
		// List of sentences
        var _CONTENT = [ 
			"Decouvrez une sélection exclusive chaque semaine", 
			"Profitez d'un prix juste",
			"Adoptez une nouvelle façon de consommer",
        ];

        // Current sentence being processed
        var _PART = 0;

        // Character number of the current sentence being processed 
        var _PART_INDEX = 0;

        // Holds the handle returned from setInterval
        var _INTERVAL_VAL;

        // Element that holds the text
        var _ELEMENT = document.querySelector("#text_animation");

        // Cursor element 
        var _CURSOR = document.querySelector("#cursor");

        // Implements typing effect
        function Type() { 
            // Get substring with 1 characater added
            var text =  _CONTENT[_PART].substring(0, _PART_INDEX + 1);
            _ELEMENT.innerHTML = text;
            _PART_INDEX++;

            // If full sentence has been displayed then start to delete the sentence after some time
            if(text === _CONTENT[_PART]) {
                // Hide the cursor
                _CURSOR.style.display = 'none';

                clearInterval(_INTERVAL_VAL);
                setTimeout(function() {
                    _INTERVAL_VAL = setInterval(Delete, 30);
                }, 1000);
            }
        }

        // Implements deleting effect
        function Delete() {
            // Get substring with 1 characater deleted
            var text =  _CONTENT[_PART].substring(0, _PART_INDEX - 1);
            _ELEMENT.innerHTML = text;
            _PART_INDEX--;

            // If sentence has been deleted then start to display the next sentence
            if(text === '') {
                clearInterval(_INTERVAL_VAL);

                // If current sentence was last then display the first one, else move to the next
                if(_PART == (_CONTENT.length - 1))
                    _PART = 0;
                else
                    _PART++;
                
                _PART_INDEX = 0;

                // Start to display the next sentence after some time
                setTimeout(function() {
                    _CURSOR.style.display = 'inline-block';
                    _INTERVAL_VAL = setInterval(Type, 70);
                }, 100);
            }
        }

        // Start the typing effect on load
        _INTERVAL_VAL = setInterval(Type, 50);

		/**** Fin code Alexandre ****/



	</script>
	{if isset($field_values) AND $field_values.bg_type}
		{if $field_values.bg_type == "background-video" AND isset($field_values.bg_video)}
			<script type="text/javascript">{literal}
				$(document).ready(function() {
					$('#wrapper').tubular({videoId: {/literal}'{$field_values.bg_video}'{literal}});
				});{/literal}
			</script>
		{/if}
	{/if}
	{if $persist == 1 AND isset($persist)}
		<script type="text/javascript">{literal}
			$(document).ready(function() {
				forgot_password();
			});{/literal}
		</script>
	{/if}
{/block}
{*</body>
</html>
*}
