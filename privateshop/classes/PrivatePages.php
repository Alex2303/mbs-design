<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file.
 * You are not authorized to modify, copy or redistribute this file.
 * Permissions are reserved by FME Modules.
 *
 *  @author    FMM Modules
 *  @copyright FME Modules 2020
 *  @license   Single domain
 */

class PrivatePages extends ObjectModel
{
    public $id;

    public $active;

    public $date_add;

    public $passwd;

    public $message;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'privateshop_protecetd_pages',
        'primary' => 'id_privateshop_protecetd_pages',
        'multilang' => true,
        'fields' => array(
            'passwd' => array('type' => self::TYPE_STRING,'validate' => 'isPasswd'),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            // multi-lingual
            'message' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
        ),
    );

    public function save($null_values = false, $auto_date = true)
    {
        if (parent::save($auto_date, $null_values)) {
            $this->updateAssoShop();
            return true;
        }
        return false;
    }

    public static function generateTables()
    {
        $sql = array();

        $sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'privateshop_protecetd_pages` (
            `id_privateshop_protecetd_pages`            INT(11) NOT NULL AUTO_INCREMENT,
            `passwd`                                    VARCHAR(255),
            `active`                                    TINYINT(2),
            `date_add`                                  DATE,
            PRIMARY KEY                                 (`id_privateshop_protecetd_pages`)
            ) ENGINE=' . _MYSQL_ENGINE_ . '             DEFAULT CHARSET=utf8;';

        $sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'privateshop_protecetd_pages_lang` (
            `id_privateshop_protecetd_pages`            INT(11) NOT NULL,
            `id_lang`                                   INT(11) NOT NULL,
            `message`                                   TEXT,
            PRIMARY KEY                                 (`id_privateshop_protecetd_pages`, `id_lang`)
            ) ENGINE=' . _MYSQL_ENGINE_ . '              DEFAULT CHARSET=utf8;';

        $sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'privateshop_protecetd_pages_shop` (
            `id_privateshop_protecetd_pages`            INT(11) NOT NULL,
            `id_shop`                                   INT(11) NOT NULL,
            `id_shop_group`                             INT(11) NOT NULL,
            PRIMARY KEY                                 (`id_privateshop_protecetd_pages`, `id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . '             DEFAULT CHARSET=utf8;';

        $sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'privateshop_protecetd_pages_condition` (
            `id_privateshop_protecetd_pages_condition`  INT(11) NOT NULL AUTO_INCREMENT,
            `id_privateshop_protecetd_pages`            INT(11) NOT NULL,
            `type`                                      VARCHAR(20) NOT NULL,
            `value`                                     INT(11) NOT NULL,
            PRIMARY KEY                                 (`id_privateshop_protecetd_pages_condition`)
            ) ENGINE=' . _MYSQL_ENGINE_ . '             DEFAULT CHARSET=utf8;';

        foreach ($sql as $query) {
            if (Db::getInstance()->execute($query) == false) {
                return false;
            }
        }
        return true;
    }

    public function delete()
    {
        $this->deletePrivatePage();
        return parent::delete();
    }

    /**
     * @param $status string
     * @param $id_privateshop_protecetd_pages int
     */
    public static function setStatus($status, $id_privateshop_protecetd_pages)
    {
        if (!$id_privateshop_protecetd_pages || empty($status)) {
            return false;
        }
        return (bool) Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . self::$definition['table'] . '
            SET `' . pSQL($status) . '` = !' . pSQL($status) . '
            WHERE id_privateshop_protecetd_pages = ' . (int) $id_privateshop_protecetd_pages);
    }

    public static function getListContent()
    {
        return Db::getInstance()->executeS('
            SELECT pp.*, ppc.`type`
            FROM `' . _DB_PREFIX_ . 'privateshop_protecetd_pages` pp
            ' . Shop::addSqlAssociation('privateshop_protecetd_pages', 'pp') . '
            LEFT JOIN `' . _DB_PREFIX_ . 'privateshop_protecetd_pages_condition` ppc
                ON (pp.`id_privateshop_protecetd_pages` = ppc.`id_privateshop_protecetd_pages`)
            GROUP BY pp.`id_privateshop_protecetd_pages`');
    }

    /**
     * @param  $id_privateshop_protecetd_pages int
     * @return array|bool
     */
    public static function getValueById($id_privateshop_protecetd_pages)
    {
        if (!$id_privateshop_protecetd_pages) {
            return false;
        }
        $sql = new DbQuery();
        $sql->select('ppc.`value`');
        $sql->from('privateshop_protecetd_pages_condition', 'ppc');
        $sql->where('ppc.`id_privateshop_protecetd_pages` = ' . (int) $id_privateshop_protecetd_pages);
        $result = Db::getInstance()->executeS($sql);

        $values = array();
        foreach ($result as $value) {
            $values[] = $value['value'];
        }
        return $values;
    }

    /**
     * Get type
     *
     * @return string|bool
     */
    public static function getPrivateType($id_privateshop_protecetd_pages)
    {
        if (!$id_privateshop_protecetd_pages) {
            return false;
        }
        $sql = new DbQuery();
        $sql->select('ppc.`type`');
        $sql->from('privateshop_protecetd_pages_condition', 'ppc');
        $sql->where('ppc.`id_privateshop_protecetd_pages` = ' . (int) $id_privateshop_protecetd_pages);
        $sql->groupBy('ppc.`id_privateshop_protecetd_pages`');
        return Db::getInstance()->getValue($sql);
    }

    public static function getProductById($id_product)
    {
        return Db::getInstance()->getRow('SELECT `id_product`, `name`
            FROM `' . _DB_PREFIX_ . 'product_lang` WHERE `id_product` = ' . (int) $id_product);
    }

    public static function searchProduct()
    {
        $query = Tools::getValue('q', false);
        if (!$query or $query == '' or Tools::strlen($query) < 1) {
            die();
        }

        /*
         * In the SQL request the "q" param is used entirely to match result in database.
         * In this way if string:"(ref : #ref_pattern#)" is displayed on the return list,
         * they are no return values just because string:"(ref : #ref_pattern#)"
         * is not write in the name field of the product.
         * So the ref pattern will be cut for the search request.
         */
        if ($pos = strpos($query, ' (ref:')) {
            $query = Tools::substr($query, 0, $pos);
        }

        $excludeIds = Tools::getValue('excludeIds', false);
        if ($excludeIds && $excludeIds != 'NaN') {
            $excludeIds = implode(',', array_map('intval', explode(',', $excludeIds)));
        } else {
            $excludeIds = '';
            $excludePackItself = Tools::getValue('packItself', false);
        }

        // Excluding downloadable products from packs because download from pack is not supported
        $excludeVirtuals = (bool) Tools::getValue('excludeVirtuals', true);
        $exclude_packs = (bool) Tools::getValue('exclude_packs', true);

        $context = Context::getContext();

        $sql = 'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, image_shop.`id_image` id_image, il.`legend`, p.`cache_default_attribute`
                FROM `' . _DB_PREFIX_ . 'product` p
                ' . Shop::addSqlAssociation('product', 'p') . '
                LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = ' . (int) $context->language->id . Shop::addSqlRestrictionOnLang('pl') . ')
                LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` image_shop
                    ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop=' . (int) $context->shop->id . ')
                LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int) $context->language->id . ')
                WHERE (pl.name LIKE \'%' . pSQL($query) . '%\' OR p.reference LIKE \'%' . pSQL($query) . '%\')' .
            (!empty($excludeIds) ? ' AND p.id_product NOT IN (' . $excludeIds . ') ' : ' ') .
            (!empty($excludePackItself) ? ' AND p.id_product <> ' . $excludePackItself . ' ' : ' ') .
            ($excludeVirtuals ? 'AND NOT EXISTS (SELECT 1 FROM `' . _DB_PREFIX_ . 'product_download` pd WHERE (pd.id_product = p.id_product))' : '') .
            ($exclude_packs ? 'AND (p.cache_is_pack IS NULL OR p.cache_is_pack = 0)' : '') .
            ' GROUP BY p.id_product';

        $items = Db::getInstance()->executeS($sql);

        if ($items) {
            foreach ($items as $item) {
                $item['name'] = str_replace('|', '&#124;', $item['name']);
                echo trim($item['name']) . (!empty($item['reference']) ? ' (ref: ' . $item['reference'] . ')' : '') . '|' . (int) ($item['id_product']) . "\n";
            }
        }
        die();
    }

    public function deletePrivatePage()
    {
        return (bool) Db::getInstance()->delete(
            'privateshop_protecetd_pages_condition',
            'id_privateshop_protecetd_pages=' . (int) $this->id
        );
    }

    public function addPrivatePage($privatePages)
    {
        if (!isset($privatePages) || !is_array($privatePages)) {
            return false;
        }

        foreach ($privatePages as $privatePage) {
            $result = Db::getInstance()->insert(
                'privateshop_protecetd_pages_condition',
                array(
                'id_privateshop_protecetd_pages' => (int) $privatePage['id_privateshop_protecetd_pages'],
                'type' => pSQL($privatePage['type']),
                'value' => pSQL($privatePage['value']),
                )
            );

            if (!$result) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param  $id_lang int
     * @param  $id_shop int
     * @return array|null
     */
    public static function getCmsPages($id_lang = null, $id_shop = null)
    {
        $id_lang = (int) (($id_lang) ?: Context::getContext()->language->id);
        $id_shop = (int) (($id_shop) ?: Context::getContext()->shop->id);

        $categories = 'SELECT  cc.`id_cms_category`,
            ccl.`name`,
            ccl.`description`,
            ccl.`link_rewrite`,
            cc.`id_parent`,
            cc.`level_depth`,
            NULL as pages
            FROM ' . _DB_PREFIX_ . 'cms_category cc
            INNER JOIN ' . _DB_PREFIX_ . 'cms_category_lang ccl
                ON (cc.`id_cms_category` = ccl.`id_cms_category`)
            INNER JOIN ' . _DB_PREFIX_ . 'cms_category_shop ccs
                ON (cc.`id_cms_category` = ccs.`id_cms_category`)
            WHERE `active` = 1
                AND ccl.`id_lang`= ' . (int) $id_lang . '
                AND ccs.`id_shop`= ' . (int) $id_shop;

        $pages = Db::getInstance()->executeS($categories);

        foreach ($pages as &$category) {
            $category['pages'] = Db::getInstance()->executeS('SELECT c.`id_cms`,
                c.`position`,
                cl.`meta_title` as title,
                cl.`meta_description` as description,
                cl.`link_rewrite`
                FROM ' . _DB_PREFIX_ . 'cms c
                INNER JOIN ' . _DB_PREFIX_ . 'cms_lang cl
                    ON (c.`id_cms` = cl.`id_cms`)
                INNER JOIN ' . _DB_PREFIX_ . 'cms_shop cs
                    ON (c.`id_cms` = cs.`id_cms`)
                WHERE c.`active` = 1
                AND c.`id_cms_category` = ' . (int) $category['id_cms_category'] . '
                AND cl.`id_lang` = ' . (int) $id_lang . '
                AND cs.`id_shop` = ' . (int) $id_shop);
        }

        return $pages;
    }

    /**
     * @param  $id_privateshop_protecetd_pages int
     * @param  $id_lang int
     * @return array|null
     */
    public static function getPrivateProducts($id_privateshop_protecetd_pages, $id_lang = null)
    {
        return Db::getInstance()->executeS('
            SELECT rc.*, p.`id_product`, p.`reference`, pl.`name`
            FROM `' . _DB_PREFIX_ . 'privateshop_protecetd_pages_condition` rc
            LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON (p.`id_product`= rc.`value`)
            ' . Shop::addSqlAssociation('product', 'p') . '
            LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (
                p.`id_product` = pl.`id_product`
                AND pl.`id_lang` = ' . (int) $id_lang . Shop::addSqlRestrictionOnLang('pl') . '
            )
            WHERE rc.type = "product"
            AND rc.`id_privateshop_protecetd_pages` = ' . (int) $id_privateshop_protecetd_pages);
    }

    public static function checkPage($page, $value, $id_lang = null, $id_shop = null)
    {
        if (empty($page) || (!empty($page) && !in_array($page, array('product', 'category', 'cms', 'meta_pages')))) {
            return false;
        }

        if (!$value) {
            return false;
        }

        if (!$id_lang) {
            $id_lang = (int) Context::getContext()->language->id;
        }

        if (!$id_shop) {
            $id_shop = (int) Context::getContext()->shop->id;
        }

        $now = date('Y-m-d');
        $sql = new DbQuery();
        $sql->select('pp.*, ppl.*, ppc.*');
        $sql->from('privateshop_protecetd_pages_condition', 'ppc');
        $sql->leftJoin(
            'privateshop_protecetd_pages',
            'pp',
            'pp.id_privateshop_protecetd_pages = ppc.id_privateshop_protecetd_pages'
        );
        $sql->leftJoin(
            'privateshop_protecetd_pages_lang',
            'ppl',
            'ppc.id_privateshop_protecetd_pages = ppl.id_privateshop_protecetd_pages AND ppl.id_lang = ' . (int) $id_lang
        );

        if (Shop::isFeatureActive()) {
            $sql->leftJoin(
                'privateshop_protecetd_pages_shop',
                'pps',
                'pp.id_privateshop_protecetd_pages = pps.id_privateshop_protecetd_pages AND pps.id_shop = '. (int) $id_shop
            );
            $sql->where('pps.id_shop = '. (int) $id_shop);
        }

        $sql->where('pp.active = 1');
        $sql->where('pp.passwd != "" OR pp.passwd IS NOT NULL');
        $sql->where('ppc.type = "' . pSQL($page) . '"');
        $sql->where('ppc.value = "' . pSQL($value) . '"');

        return Db::getInstance()->getRow($sql);
    }

    /**
     * Update the associations of shops.
     *
     * @param int $id_object
     *
     * @return bool|void
     *
     * @throws PrestaShopDatabaseException
     */
    protected function updateAssoShop()
    {
        if (!Shop::isFeatureActive()) {
            return;
        }

        if (!Shop::isTableAssociated(self::$definition['table'])) {
            return;
        }

        $assos_data = $this->getSelectedAssoShop();

        // Get list of shop id we want to exclude from asso deletion
        $exclude_ids = $assos_data;
        foreach (Db::getInstance()->executeS('SELECT id_shop FROM ' . _DB_PREFIX_ . 'shop') as $row) {
            if (!Context::getContext()->employee->hasAuthOnShop($row['id_shop'])) {
                $exclude_ids[] = $row['id_shop'];
            }
        }
        Db::getInstance()->delete(self::$definition['table'] . '_shop', '`' . bqSQL(self::$definition['primary']) . '` = ' . (int) $this->id . ($exclude_ids ? ' AND id_shop NOT IN (' . implode(', ', array_map('intval', $exclude_ids)) . ')' : ''));

        $insert = array();
        foreach ($assos_data as $id_shop) {
            $insert[] = array(
                self::$definition['primary'] => (int) $this->id,
                'id_shop' => (int) $id_shop,
            );
        }

        return Db::getInstance()->insert(self::$definition['table'] . '_shop', $insert, false, true, Db::INSERT_IGNORE);
    }

    /**
     * Returns an array with selected shops and type (group or boutique shop).
     *
     * @param string $table
     *
     * @return array
     */
    protected function getSelectedAssoShop()
    {
        if (!Shop::isFeatureActive() || !Shop::isTableAssociated(self::$definition['table'])) {
            return array();
        }

        $shops = Shop::getShops(true, null, true);
        if (count($shops) == 1 && isset($shops[0])) {
            return array($shops[0], 'shop');
        }

        $assos = array();
        if (Tools::isSubmit('checkBoxShopAsso_' . self::$definition['table'])) {
            foreach (Tools::getValue('checkBoxShopAsso_' . self::$definition['table']) as $id_shop => $value) {
                $assos[] = (int) $id_shop;
            }
        } elseif (Shop::getTotalShops(false) == 1) {
            // if we do not have the checkBox multishop, we can have an admin with only one shop and being in multishop
            $assos[] = (int) Shop::getContextShopID();
        }

        return $assos;
    }

    public function getAssocShops()
    {
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT id_shop
            FROM '._DB_PREFIX_.self::$definition['table'].'_shop
            WHERE '. self::$definition['primary'] .'='. $this->id);

        $final = array();
        if (isset($result)) {
            foreach ($result as $res) {
                $final[] = $res['id_shop'];
            }
        }
        return $final;
    }
}
