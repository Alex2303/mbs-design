<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file.
 * You are not authorized to modify, copy or redistribute this file.
 * Permissions are reserved by FME Modules.
 *
 *  @author    FMM Modules
 *  @copyright FME Modules 2020
 *  @license   Single domain
 */

class PrivateAcces extends ObjectModel
{
    /**
     * create essential database tables
     * @return bool
     */
    public static function createTable()
    {
        $return = true;
        $return &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'privateshop_shop`(
            `id_shop`                           int(10) unsigned NOT NULL,
            `id_group`                          int(10) unsigned NOT NULL,
            PRIMARY KEY                         (`id_shop`, `id_group`))
            ENGINE=' . _MYSQL_ENGINE_ . '       DEFAULT CHARSET=utf8'
        );
        $return &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'privateshop_urls`(
            `id_privateshop_urls`               int(11) NOT NULL auto_increment,
            `url`                               varchar(255) NOT NULL,
            PRIMARY KEY                         (`id_privateshop_urls`))
            ENGINE=' . _MYSQL_ENGINE_ . '       DEFAULT CHARSET=utf8'
        );
        $return &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'privateshop_urls_restricted`(
            `id_privateshop_urls_restricted`    int(11) NOT NULL auto_increment,
            `url`                               varchar(255) NOT NULL,
            PRIMARY KEY                         (`id_privateshop_urls_restricted`))
            ENGINE=' . _MYSQL_ENGINE_ . '       DEFAULT CHARSET=utf8'
        );
        return $return;
    }

    /**
     * drop essential tables
     * @return bool
     */
    public static function dropTables()
    {
        $return = true;
        $return &= Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'privateshop_shop`');
        $return &= Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'privateshop_urls`');
        $return &= Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'privateshop_urls_restricted`');
        return $return;
    }

    /**
     * clear all whitelisted urls
     * @return bool
     */
    public static function flushWhitelistUrls()
    {
        return (bool) Db::getInstance()->delete('privateshop_urls');
    }

    /**
     * add whitelist urls
     * @param string    $url
     */
    public static function addWhitelistUrl($url)
    {
        return (bool) Db::getInstance()->insert(
            'privateshop_urls',
            array(
                'url' => $url,
            )
        );
    }

    /**
     * clear all blacklist urls
     * @return bool
     */
    public static function flushBlacklistUrls()
    {
        return (bool) Db::getInstance()->delete('privateshop_urls_restricted');
    }

    /**
     * add whitelist urls
     * @param string    $url
     */
    public static function addBlacklistUrl($url)
    {
        return (bool) Db::getInstance()->insert(
            'privateshop_urls_restricted',
            array(
                'url' => $url,
            )
        );
    }

    /**
     * get all whitelisted URLs
     * @return null|string
     */
    public static function getAllWhitelistUrls()
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM ' . _DB_PREFIX_ . 'privateshop_urls');
    }

    /**
     * get all restricted URLs
     * @return null|string
     */
    public static function getAllRestrictedUrls()
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT * FROM ' . _DB_PREFIX_ . 'privateshop_urls_restricted');
    }

    /**
     * check URL for restriction
     * @param string    $url
     * @return bool
     */
    public static function checkIfBlacklistedUrl($url)
    {
        return (bool) Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('SELECT *
            FROM ' . _DB_PREFIX_ . 'privateshop_urls_restricted
            WHERE `url` = "' . pSQL($url) . '"');
    }

    /**
     * check URL for whitelist
     * @param string    $url
     * @return bool
     */
    public static function checkIfWhitelistedUrl($url)
    {
        return (bool) Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('SELECT *
            FROM ' . _DB_PREFIX_ . 'privateshop_urls
            WHERE `url` = "' . pSQL($url) . '"');
    }

    /**
     * get all id_shop of private shops
     * @return array
     */
    public static function getAssocShops()
    {
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT id_shop FROM ' . _DB_PREFIX_ . 'privateshop_shop');
        $final = array();
        if (isset($result)) {
            foreach ($result as $res) {
                $final[] = $res['id_shop'];
            }
        }
        return $final;
    }

    /**
     * get customer email by id and token
     * @param string    $email
     * @param string    $token
     * @return string
     */
    public static function getCustomerEmail($id_customer, $token)
    {
        if (!$id_customer || empty($token)) {
            return false;
        }

        return Db::getInstance()->getValue('SELECT `email`
            FROM ' . _DB_PREFIX_ . 'customer c
            WHERE c.`secure_key` = \'' . pSQL($token) . '\'
            AND c.id_customer = ' . (int) $id_customer
        );
    }

    /**
     * get all shop customers
     * @return array
     */
    public static function getAllCustomers()
    {
        $like = '';
        $context = Context::getContext();
        $search_n = ((int) Configuration::get('PRIVATESHOP_FILTER_n', false, $context->shop->id_shop_group, $context->shop->id) <= 0) ? 10 : (int) Configuration::get('PRIVATESHOP_FILTER_n', false, $context->shop->id_shop_group, $context->shop->id);
        $search_pos = (int) Configuration::get('PRIVATESHOP_FILTER_pos', false, $context->shop->id_shop_group, $context->shop->id);
        $search_pos = ($search_pos <= 0) ? ' ORDER BY c.id_customer ASC ' : 'ORDER BY c.id_customer DESC ';
        $search_state = (int) Configuration::get('PRIVATESHOP_FILTER_state', false, $context->shop->id_shop_group, $context->shop->id);
        $name = Configuration::get('PRIVATESHOP_FILTER_name', false, $context->shop->id_shop_group, $context->shop->id);
        if (empty($name)) {
            $like = '';
        } elseif (!empty($name) && ($search_state == 1 || $search_state == 2)) {
            $like = 'AND c.firstname LIKE "%' . pSQL($name) . '%" OR c.lastname LIKE "%' . pSQL($name) . '%" ';
        } elseif (!empty($name) && $search_state <= 0) {
            $like = 'WHERE c.firstname LIKE "%' . pSQL($name) . '%" OR c.lastname LIKE "%' . pSQL($name) . '%" ';
        }

        if ($search_state <= 0) {
            $search_state = '';
        } elseif ($search_state == 1) {
            $search_state = 'WHERE c.active = 1 ';
        } elseif ($search_state == 2) {
            $search_state = 'WHERE c.active = 0 ';
        }
        $sql = 'SELECT c.*, CONCAT(LEFT(c.`firstname`, 1), \'. \', c.`lastname`) `customer`, gl.`name` AS `title`,
            (SELECT co.`date_add` FROM ' . _DB_PREFIX_ . 'guest g
                LEFT JOIN ' . _DB_PREFIX_ . 'connections co ON co.id_guest = g.id_guest
                WHERE g.id_customer = c.id_customer
                ORDER BY c.date_add DESC
                LIMIT 1
                ) as connect
            FROM ' . _DB_PREFIX_ . 'customer c
            LEFT JOIN ' . _DB_PREFIX_ . 'gender_lang gl ON (c.id_gender = gl.id_gender AND gl.id_lang = ' . (int) Context::getContext()->language->id . ')
            ' . $search_state . $like . $search_pos . 'LIMIT ' . (int) $search_n;

        return Db::getInstance()->executeS($sql);
    }

    /**
     * Get all metas.
     *
     * @param int $idLang
     *
     * @return array|false|mysqli_result|PDOStatement|resource|null
     */
    public static function getAllMeta($idLang)
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
        SELECT *
        FROM ' . _DB_PREFIX_ . 'meta m
        LEFT JOIN ' . _DB_PREFIX_ . 'meta_lang ml ON m.id_meta = ml.id_meta
        AND ml.id_lang = ' . (int) $idLang . '
        ' . Shop::addSqlRestrictionOnLang('ml'));
    }
}
