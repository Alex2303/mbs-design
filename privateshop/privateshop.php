<?php
/**
 * PrivateShop
 *
 * Do not edit or add to this file.
 * You are not authorized to modify, copy or redistribute this file.
 * Permissions are reserved by FME Modules.
 *
 *  @author    FME Modules
 *  @copyright 2020 FME Modules All right reserved
 *  @license   Copyrights FME Modules
 *  @category  FMM Modules
 *  @package   PrivateShop
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

if (!defined('_MYSQL_ENGINE_')) {
    define('_MYSQL_ENGINE_', 'MyISAM');
}

include_once(_PS_MODULE_DIR_ . 'privateshop/lib/simple_html_dom.php');

include_once(_PS_MODULE_DIR_ . 'privateshop/classes/PrivateAcces.php');

include_once(_PS_MODULE_DIR_ . 'privateshop/classes/PrivatePages.php');

class PrivateShop extends Module
{
    public $translations = array();

    public static $currentFormTab;

    protected $id_shop = null;

    protected $id_shop_group = null;

    public function __construct()
    {
        $this->name = 'privateshop';
        $this->tab = 'administration';
        $this->version = '1.7.1';
        $this->author = 'FMM Modules';
        $this->bootstrap = true;
        $this->module_key = '87ab9c57883b99614ee87cacdac232fe';
        $this->author_address = '0xcC5e76A6182fa47eD831E43d80Cd0985a14BB095';

        parent::__construct();

        $this->displayName = $this->l('Private Shop');
        $this->description = $this->l('This module allows you to restrict your online store to accessible by registered customers only.');

        $this->translations = $this->getPrivateShopTranslations();

        $this->setShopIdx();
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        include dirname(__FILE__) . '/sql/install.php';

        if (parent::install() &&
            $this->installConfiguration() &&
            $this->registerHook('header') &&
            $this->registerHook('footer') &&
            $this->registerHook('ModuleRoutes') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('actionCustomerAccountAdd') &&
            $this->installTab('AdminPrivatePages', 'Private Pages')) {
            return true;
        }
        return false;
    }

    public function uninstall()
    {
        include dirname(__FILE__) . '/sql/uninstall.php';

        if (parent::uninstall()
            && $this->uninstallTab()
            && $this->uninstallConfiguration()
            && $this->unregisterHook('header')
            && $this->unregisterHook('footer')
            && $this->unregisterHook('ModuleRoutes')
            && $this->unregisterHook('backOfficeHeader')
            && $this->unregisterHook('actionCustomerAccountAdd')) {
            return true;
        }
        return false;
    }

    public function getContent()
    {
        $this->postProcess();
        if (Tools::isSubmit('addprivateshop_protecetd_pages') || Tools::isSubmit('updateprivateshop_protecetd_pages')) {
            return $this->renderPasswordPageForm();
        }
        $this->html = $this->display(__FILE__, 'views/templates/hook/info.tpl');
        return $this->html . $this->displayForm();
    }

    public function getAvailableControllers($by_page_id = false)
    {
        $pages = array();
        $files = Meta::getPages(false);
        if (isset($files) && $files) {
            foreach ($files as $file) {
                $type = (preg_match('#^module-#', $file)) ? 'module' : false;
                if (isset($type) && $type) {
                    $module_pages = explode('-', $file);
                    if (isset($module_pages) && $module_pages) {
                        $module_name = isset($module_pages[1]) && $module_pages[1] ? $module_pages[1] : false;
                        if ($module_name && Validate::isModuleName($module_name) && $module_name != $this->name && Module::isInstalled($module_name)) {
                            if ($by_page_id) {
                                $pages[$file] = $module_pages[2];
                            } else {
                                $pages[$module_name][$file] = $module_pages[2];
                            }
                        }
                    }
                }
            }
        }
        return $pages;
    }

    private function postProcess()
    {
        if (Tools::isSubmit('saveConfiguration')) {
            $languages = Language::getLanguages(false);
            $values = array();
            foreach ($languages as $lang) {
                $values['LOGIN_TITLE'][$lang['id_lang']] = Tools::getValue('LOGIN_TITLE_' . $lang['id_lang']);
                $values['SIGNUP_TITLE'][$lang['id_lang']] = Tools::getValue('SIGNUP_TITLE_' . $lang['id_lang']);
                $values['PRIVATE_RESTRICT_MESSAGE'][$lang['id_lang']] = Tools::getValue('restrict_message_' . $lang['id_lang'], true);
                $values['PRIVATE_CUSTOMER_GROUP_MSG'][$lang['id_lang']] = Tools::getValue('PRIVATE_CUSTOMER_GROUP_MSG_' . $lang['id_lang']);
            }
            //General Configurations
            $form_position = (string) Tools::getValue('FORM_POSITION');
            $private_signup = (int) Tools::getValue('PRIVATE_SIGNUP');
            $background_type = (string) Tools::getValue('BACKGROUND_TYPE');
            $background_color = (string) Tools::getValue('BACKGROUND_COLOR');
            $bg_image = (string) Tools::getValue('bg_image_selected');
            $bg_video_img = (string) Tools::getValue('bg_video_img');
            $custom_logo_img = (string) Tools::getValue('custom_logo_img');

            //Access Control Configurations
            $privatize_shop = (string) Tools::getValue('PRIVATIZE_SHOP');
            $allowed_ip = (string) Tools::getValue('ACCESS_GRANTED_IP');
            $private_products = (Tools::getValue('private_products')) ? implode(',', Tools::getValue('private_products')) : '';
            $category_box = (Tools::getValue('categoryBox')) ? implode(',', Tools::getValue('categoryBox')) : '';
            $cms_pages = (Tools::getValue('cms_pages')) ? implode(',', Tools::getValue('cms_pages')) : '';
            //module-controllers
            $module_pages = Tools::getIsset('MODULE_PAGES') && Tools::getValue('MODULE_PAGES') ? implode(',', Tools::getValue('MODULE_PAGES')) : '';
            if ($_FILES && $_FILES['bg_image']['name']) {
                $img_real_name = $_FILES['bg_image']['name'];

                if (!file_exists(_PS_MODULE_DIR_ . $this->name . '/views/img/private')) {
                    mkdir(_PS_MODULE_DIR_ . $this->name . '/views/img/private', 0777, true);
                }
                if (move_uploaded_file($_FILES['bg_image']['tmp_name'], _PS_MODULE_DIR_ . $this->name . '/views/img/private/' . (string) $img_real_name)) {
                    $bg_image = $_FILES['bg_image']['name'];
                }
            }
            //Temp img for Youtube vid BG
            if ($_FILES && $_FILES['bg_video_img']['name'] && $background_type == 'background-video') {
                $img_real_name = $_FILES['bg_video_img']['name'];

                if (!file_exists(_PS_MODULE_DIR_ . $this->name . '/views/img/private/tmp')) {
                    mkdir(_PS_MODULE_DIR_ . $this->name . '/views/img/private/tmp', 0777, true);
                }
                if (move_uploaded_file($_FILES['bg_video_img']['tmp_name'], _PS_MODULE_DIR_ . $this->name . '/views/img/private/tmp/' . (string) $img_real_name)) {
                    $bg_video_img = $_FILES['bg_video_img']['name'];
                }
            }
            //logo for custom
            if ($_FILES && $_FILES['custom_logo_img']['name']) {
                $img_real_name = $_FILES['custom_logo_img']['name'];

                if (!file_exists(_PS_MODULE_DIR_ . $this->name . '/views/img/private/tmp')) {
                    mkdir(_PS_MODULE_DIR_ . $this->name . '/views/img/private/tmp', 0777, true);
                }
                if (move_uploaded_file($_FILES['custom_logo_img']['tmp_name'], _PS_MODULE_DIR_ . $this->name . '/views/img/private/tmp/' . (string) $img_real_name)) {
                    $custom_logo_img = $_FILES['custom_logo_img']['name'];
                }
            }

            Configuration::updateValue('MODULE_PAGES', $module_pages, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATIZE_SHOP', $privatize_shop, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('LOGIN_TITLE', $values['LOGIN_TITLE'], null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('SIGNUP_TITLE', $values['SIGNUP_TITLE'], null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_RESTRICT_MESSAGE', $values['PRIVATE_RESTRICT_MESSAGE'], true, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_CUSTOMER_GROUP_MSG', $values['PRIVATE_CUSTOMER_GROUP_MSG'], true, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('FORM_POSITION', $form_position, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_SIGNUP', $private_signup, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('BACKGROUND_TYPE', $background_type, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('BACKGROUND_COLOR', $background_color, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('bg_image', $bg_image, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('ACCESS_GRANTED_IP', $allowed_ip, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('private_products', $private_products, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('categoryBox', $category_box, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('cms_pages', $cms_pages, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_SIGNUP_RESTRICT', (int) Tools::getValue('PRIVATE_SIGNUP_RESTRICT'), null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_RESTRICT_GOOGLE', (int) Tools::getValue('PRIVATE_RESTRICT_GOOGLE'), null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_SHOW_STORE_TITLE', (int) Tools::getValue('PRIVATE_SHOW_STORE_TITLE'), null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_SHOW_HEADER_FOOTER', (int) Tools::getValue('PRIVATE_SHOW_HEADER_FOOTER'), null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_FORM_THEME', Tools::getValue('PRIVATE_FORM_THEME'), null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('BG_OPACITY', Tools::getValue('BG_OPACITY'), null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_BDAY', Tools::getValue('PRIVATE_BDAY'), null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_GENDER_OPT', Tools::getValue('PRIVATE_GENDER_OPT'), null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_NLETTER_OPT', Tools::getValue('PRIVATE_NLETTER_OPT'), null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_OFFERS_OPT', Tools::getValue('PRIVATE_OFFERS_OPT'), null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_CUSTOM_LOGO', Tools::getValue('PRIVATE_CUSTOM_LOGO'), null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('BACKGROUND_VIDEO', Tools::getValue('BACKGROUND_VIDEO'), null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATE_CUSTOMER_GROUP_STATE', (int) Tools::getValue('PRIVATE_CUSTOMER_GROUP_STATE'), null, $this->id_shop_group, $this->id_shop);

            if ($_FILES && $_FILES['bg_video_img']['name'] && $background_type == 'background-video') {
                Configuration::updateValue('BACKGROUND_VIDEO_IMG', $bg_video_img, null, $this->id_shop_group, $this->id_shop);
            }
            if ($_FILES && $_FILES['custom_logo_img']['name']) {
                Configuration::updateValue('PRIVATE_CUSTOM_LOGO_IMG', $custom_logo_img, null, $this->id_shop_group, $this->id_shop);
            }
            //Save URLs whitelisted
            PrivateAcces::flushWhitelistUrls();
            $whitelisted_urls = Tools::getValue('whiteurls');
            if (!empty($whitelisted_urls)) {
                foreach ($whitelisted_urls as $url) {
                    if (!empty($url)) {
                        PrivateAcces::addWhitelistUrl($url);
                    }
                }
            }
            //Save URLs restricted
            $restricturls = Tools::getValue('restricturls');
            PrivateAcces::flushBlacklistUrls();
            if (!empty($restricturls)) {
                foreach ($restricturls as $url) {
                    if (!empty($url)) {
                        PrivateAcces::addBlacklistUrl($url);
                    }
                }
            }
            //Save Customer Groups
            $groups = Tools::getValue('groups');
            $groups = is_array($groups) ? implode(',', $groups) : '';
            Configuration::updateValue('PRIVATE_CUSTOMERS_GROUPS', $groups, null, $this->id_shop_group, $this->id_shop);
            Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&conf=4&token=' . Tools::getAdminTokenLite('AdminModules'));
        } elseif (Tools::isSubmit('activatecustomer')) {
            $id_customer = (int) Tools::getValue('id_customer');
            if ($id_customer > 0) {
                $customer = new Customer($id_customer);
                if ((int) $customer->active <= 0) {
                    $customer->active = 1;
                    $customer->update();
                    $this->sendMailCustomerAccount($customer);
                }
            }
        } elseif (Tools::isSubmit('search')) {
            $n = ((int) Tools::getValue('n') > 10) ? (int) Tools::getValue('n') : 10;
            $sort = (int) Tools::getValue('filter_select_pos');
            $state = (int) Tools::getValue('filter_select_state');
            $name = Tools::getValue('search_by_name');
            Configuration::updateValue('PRIVATESHOP_FILTER_n', $n, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATESHOP_FILTER_pos', $sort, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATESHOP_FILTER_state', $state, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATESHOP_FILTER_name', $name, null, $this->id_shop_group, $this->id_shop);
            Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&conf=4&tab_module=customers&token=' . Tools::getAdminTokenLite('AdminModules'));
        } elseif (Tools::isSubmit('searchReset')) {
            Configuration::updateValue('PRIVATESHOP_FILTER_n', 10, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATESHOP_FILTER_pos', 0, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATESHOP_FILTER_state', 0, null, $this->id_shop_group, $this->id_shop);
            Configuration::updateValue('PRIVATESHOP_FILTER_name', '', null, $this->id_shop_group, $this->id_shop);
            Tools::redirectAdmin(AdminController::$currentIndex . '&configure=' . $this->name . '&conf=4&tab_module=customers&token=' . Tools::getAdminTokenLite('AdminModules'));
        } elseif (Tools::isSUbmit('submitPasswdPages')) {
            static::$currentFormTab = Tools::getValue('currentFormTab');
            return $this->submitPrivatePage();
        } elseif (Tools::isSubmit('statusprivateshop_protecetd_pages')) {
            static::$currentFormTab = 'passwordpage';
            if (!PrivatePages::setStatus('active', (int)Tools::getValue('id_privateshop_protecetd_pages'))) {
                $this->context->controller->errors[] = $this->l('Status update failed.');
            } else {
                $this->context->controller->confirmations[] = $this->l('Status updated successfully.');
            }
        } elseif (Tools::isSubmit('deleteprivateshop_protecetd_pages')) {
            static::$currentFormTab = 'passwordpage';
            if (!Validate::isLoadedObject($privatePages = new PrivatePages((int)Tools::getValue('id_privateshop_protecetd_pages')))) {
                $this->context->controller->errors[] = $this->l('Object not found.');
            } else {
                if (!$privatePages->delete()) {
                    $this->context->controller->errors[] = $this->l('Record cannot be deleted.');
                } else {
                    $this->context->controller->confirmations[] = $this->l('Record deleted successfully.');
                }
            }
        } elseif (Tools::isSubmit('submitBulkdeleteprivateshop_protecetd_pages')) {
            static::$currentFormTab = 'passwordpage';
            $privateshopProtecetdPagesBox = Tools::getValue('privateshop_protecetd_pagesBox');
            if (isset($privateshopProtecetdPagesBox) && $privateshopProtecetdPagesBox) {
                $deleted = 0;
                foreach ($privateshopProtecetdPagesBox as $id_privateshop_protecetd_pages) {
                    if (!Validate::isLoadedObject($privatePages = new PrivatePages((int)$id_privateshop_protecetd_pages))) {
                        $this->context->controller->errors[] = $this->l('Object not found.');
                        break;
                    } else {
                        if ($privatePages->delete()) {
                            $deleted++;
                        }
                    }
                }
                if (count($this->context->controller->errors)) {
                    $this->context->controller->errors;
                } else {
                    $this->context->controller->confirmations[] = sprintf($this->l('%s records deleted successfully.'), $deleted);
                }
            }
        }
    }

    public function installConfiguration()
    {
        Configuration::updateValue('bg_image', '', null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('cms_pages', '', null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('PS_JS_DEFER', 0, null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('categoryBox', '', null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('MODULE_PAGES', '', null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('PRIVATE_SIGNUP', 1, null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('private_products', '', null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('ACCESS_GRANTED_IP', '', null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('FORM_POSITION', 'center', null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('PRIVATE_FORM_THEME', 'mod', null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('BACKGROUND_COLOR', '#0085a9', null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('PRIVATE_SHOW_STORE_TITLE', 1, null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('PRIVATIZE_SHOP', 'whole-shop', null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('PRIVATE_SHOW_HEADER_FOOTER', 0, null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('PRIVATE_CUSTOMERS_GROUPS', '3', null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('PRIVATE_CUSTOMER_GROUP_STATE', 0, null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('BACKGROUND_TYPE', 'background-color', null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('LOGIN_TITLE', array($this->context->language->id => $this->l('Private Login')), null, $this->id_shop_group, $this->id_shop);
        Configuration::updateValue('SIGNUP_TITLE', array($this->context->language->id => $this->l('Private Signup')), null, $this->id_shop_group, $this->id_shop);
        return true;
    }

    public function uninstallConfiguration()
    {
        Configuration::deleteByName('cms_pages');
        Configuration::deleteByName('bg_image');
        Configuration::deleteByName('categoryBox');
        Configuration::deleteByName('PS_JS_DEFER');
        Configuration::deleteByName('MODULE_PAGES');
        Configuration::deleteByName('LOGIN_TITLE');
        Configuration::deleteByName('SIGNUP_TITLE');
        Configuration::deleteByName('FORM_POSITION');
        Configuration::deleteByName('PRIVATIZE_SHOP');
        Configuration::deleteByName('PRIVATE_SIGNUP');
        Configuration::deleteByName('BACKGROUND_TYPE');
        Configuration::deleteByName('BACKGROUND_COLOR');
        Configuration::deleteByName('private_products');
        Configuration::deleteByName('ACCESS_GRANTED_IP');
        Configuration::deleteByName('PRIVATE_FORM_THEME');
        Configuration::deleteByName('PRIVATE_SHOW_STORE_TITLE');
        Configuration::deleteByName('PRIVATE_CUSTOMERS_GROUPS');
        Configuration::deleteByName('PRIVATE_SHOW_HEADER_FOOTER');
        Configuration::deleteByName('PRIVATE_CUSTOMER_GROUP_STATE');
        return true;
    }

    public function getConfigurationValues()
    {
        $languages = Language::getLanguages(false);
        $fields = array();
        foreach ($languages as $lang) {
            $fields['login_title'][$lang['id_lang']] = Tools::getValue('LOGIN_TITLE_' . $lang['id_lang'], Configuration::get('LOGIN_TITLE', $lang['id_lang']));
            $fields['signup_title'][$lang['id_lang']] = Tools::getValue('SIGNUP_TITLE_' . $lang['id_lang'], Configuration::get('SIGNUP_TITLE', $lang['id_lang']));
            $fields['restrict_message'][$lang['id_lang']] = Tools::getValue('restrict_message_' . $lang['id_lang'], Configuration::get('PRIVATE_RESTRICT_MESSAGE', $lang['id_lang']));
            $fields['cg_mesg'][$lang['id_lang']] = Tools::getValue('PRIVATE_CUSTOMER_GROUP_MSG_' . $lang['id_lang'], Configuration::get('PRIVATE_CUSTOMER_GROUP_MSG', $lang['id_lang']));
        }

        $theme = (empty(Configuration::get('PRIVATE_FORM_THEME', false, $this->id_shop_group, $this->id_shop))) ? 'mod' : Configuration::get('PRIVATE_FORM_THEME', false, $this->id_shop_group, $this->id_shop);
        $conf_values = array(
            'priv_form_theme' => $theme,
            'cg_mesg' => $fields['cg_mesg'],
            'login_title' => $fields['login_title'],
            'signup_title' => $fields['signup_title'],
            'restrict_message' => $fields['restrict_message'],
            'pages' => Configuration::get('cms_pages', false, $this->id_shop_group, $this->id_shop),
            'bg_img' => Configuration::get('bg_image', false, $this->id_shop_group, $this->id_shop),
            'bday' => Configuration::get('PRIVATE_BDAY', false, $this->id_shop_group, $this->id_shop),
            'category' => Configuration::get('categoryBox', false, $this->id_shop_group, $this->id_shop),
            'bg_opacity' => Configuration::get('BG_OPACITY', false, $this->id_shop_group, $this->id_shop),
            'position' => Configuration::get('FORM_POSITION', false, $this->id_shop_group, $this->id_shop),
            'bg_type' => Configuration::get('BACKGROUND_TYPE', false, $this->id_shop_group, $this->id_shop),
            'bg_color' => Configuration::get('BACKGROUND_COLOR', false, $this->id_shop_group, $this->id_shop),
            'bg_video' => Configuration::get('BACKGROUND_VIDEO', false, $this->id_shop_group, $this->id_shop),
            'products' => Configuration::get('private_products', false, $this->id_shop_group, $this->id_shop),
            'active_signup' => Configuration::get('PRIVATE_SIGNUP', false, $this->id_shop_group, $this->id_shop),
            'allowed_ip' => Configuration::get('ACCESS_GRANTED_IP', false, $this->id_shop_group, $this->id_shop),
            'offers_opt' => Configuration::get('PRIVATE_OFFERS_OPT', false, $this->id_shop_group, $this->id_shop),
            'gender_opt' => Configuration::get('PRIVATE_GENDER_OPT', false, $this->id_shop_group, $this->id_shop),
            'PRIVATIZE_SHOP' => Configuration::get('PRIVATIZE_SHOP', false, $this->id_shop_group, $this->id_shop),
            'groups' => Configuration::get('PRIVATE_CUSTOMERS_GROUPS', false, $this->id_shop_group, $this->id_shop),
            'custom_logo' => Configuration::get('PRIVATE_CUSTOM_LOGO', false, $this->id_shop_group, $this->id_shop),
            'nletter_opt' => Configuration::get('PRIVATE_NLETTER_OPT', false, $this->id_shop_group, $this->id_shop),
            'bg_video_img' => Configuration::get('BACKGROUND_VIDEO_IMG', false, $this->id_shop_group, $this->id_shop),
            'custom_logo_img' => Configuration::get('PRIVATE_CUSTOM_LOGO_IMG', false, $this->id_shop_group, $this->id_shop),
            'active_google' => (int) Configuration::get('PRIVATE_RESTRICT_GOOGLE', false, $this->id_shop_group, $this->id_shop),
            'show_store_title' => (int) Configuration::get('PRIVATE_SHOW_STORE_TITLE', false, $this->id_shop_group, $this->id_shop),
            'cgroup_active' => (int) Configuration::get('PRIVATE_CUSTOMER_GROUP_STATE', false, $this->id_shop_group, $this->id_shop),
            'show_header_footer' => (int) Configuration::get('PRIVATE_SHOW_HEADER_FOOTER', false, $this->id_shop_group, $this->id_shop),
            'active_signup_restrict' => (int) Configuration::get('PRIVATE_SIGNUP_RESTRICT', false, $this->id_shop_group, $this->id_shop),
        );
        return $conf_values;
    }

    public function displayForm()
    {
        $field_values = $this->getConfigurationValues();
        $module_pages = $this->getAvailableControllers();
        $white_list_urls = PrivateAcces::getAllWhitelistUrls();
        $restrict_urls = PrivateAcces::getAllRestrictedUrls();
        $token = Tools::getAdminTokenLite('AdminModules');
        $module_link = $this->context->link->getAdminLink('AdminModules', false);
        $url = $module_link . '&configure=privateshop&token=' . $token . '&tab_module=administration&module_name=privateshop';
        $category_tree = '';
        $root = Category::getRootCategory();
        $customers = PrivateAcces::getAllCustomers();
        $customer_groups = Group::getGroups($this->context->language->id);
        foreach ($customer_groups as $key => $group) {
            if ($group['id_group'] <= 2) {
                unset($customer_groups[$key]);
            }
        }
        $languages = Language::getLanguages(false);
        if (Tools::version_compare(_PS_VERSION_, '1.6.0.0', '>=')) {
            $tree = new HelperTreeCategories('associated-categories-tree', $this->l('Associated categories'));
            $tree->setRootCategory($root->id);
            $tree->setUseCheckBox(true);
            $tree->setUseSearch(true);
            if (isset($field_values) && !empty($field_values['category'])) {
                $selected_categories = explode(',', $field_values['category']);
                $tree->setSelectedCategories($selected_categories);
            }
            $category_tree = $tree->render();
        } else {
            $tree = new Helper();
            $category_tree = $tree->renderCategoryTree(
                null,
                (isset($field_values) && !empty($field_values['category']) ? explode(',', $field_values['category']) : array()),
                'categoryBox[]',
                true,
                true,
                array(),
                false,
                false
            );
        }

        $products = array();
        if (isset($field_values) && !empty($field_values['products'])) {
            $products = explode(',', $field_values['products']);
        }
        $pages = array();
        if (isset($field_values) && !empty($field_values['pages'])) {
            $pages = explode(',', $field_values['pages']);
        }
        $selected_groups = array();
        if (isset($field_values) && !empty($field_values['groups'])) {
            $selected_groups = explode(',', $field_values['groups']);
        }
        $module_controllers = Configuration::get('MODULE_PAGES', false, $this->id_shop_group, $this->id_shop) && !empty(Configuration::get('MODULE_PAGES', false, $this->id_shop_group, $this->id_shop)) ? explode(',', Configuration::get('MODULE_PAGES', false, $this->id_shop_group, $this->id_shop)) : array();
        $iso_tiny_mce = $this->context->language->iso_code;
        $iso_tiny_mce = (file_exists(_PS_JS_DIR_ . 'tiny_mce/langs/' . $iso_tiny_mce . '.js') ? $iso_tiny_mce : 'en');
        
        $this->context->controller->addJS(array(
            _PS_JS_DIR_ . 'tiny_mce/tiny_mce.js',
            _PS_JS_DIR_ . 'admin/tinymce.inc.js',
        ));

        $activate_customer = (int) Tools::getValue('activatecustomer');
        $tab_select = (empty(Tools::getValue('tab_module'))) ? 'administration' : Tools::getValue('tab_module');
        $search = array(
            'n' => (int) Configuration::get('PRIVATESHOP_FILTER_n', false, $this->id_shop_group, $this->id_shop),
            'pos' => (int) Configuration::get('PRIVATESHOP_FILTER_pos', false, $this->id_shop_group, $this->id_shop),
            'state' => (int) Configuration::get('PRIVATESHOP_FILTER_state', false, $this->id_shop_group, $this->id_shop),
            'name' => Configuration::get('PRIVATESHOP_FILTER_name', false, $this->id_shop_group, $this->id_shop),
        );

        $this->context->smarty->assign(array(
            'version' => _PS_VERSION_,
            'URL' => $url,
            'pages' => $pages,
            'module' => new PrivateShop,
            'categories' => $category_tree,
            'link' => $this->context->link,
            'admin_dir' => _PS_ADMIN_DIR_,
            'customers' => $customers,
            'products' => $products,
            'languages' => $languages,
            'version' => _PS_VERSION_,
            'tab_select' => $tab_select,
            'search_result' => $search,
            'groups' => $customer_groups,
            'iso_tiny_mce' => $iso_tiny_mce,
            'field_values' => $field_values,
            'module_pages' => $module_pages,
            'images' => $this->readImageDir(),
            'restrict_urls' => $restrict_urls,
            'cur_ip' => Tools::getRemoteAddr(),
            'whitelist_urls' => $white_list_urls,
            'selected_groups' => $selected_groups,
            'activate_customer' => $activate_customer,
            'currentFormTab' => static::$currentFormTab,
            'module_controllers' => $module_controllers,
            'ad' => __PS_BASE_URI__ . basename(_PS_ADMIN_DIR_),
            'active_lang' => (int) $this->context->language->id,
            'ctoken' => Tools::getAdminTokenLite('AdminCustomers'),
            'pwd_pages' => $this->renderPasswordProtectedPagesList(),
            'cms_pages' => CMS::getLinks((int) $this->context->language->id),
            'cIndex' => $this->context->link->getAdminLink('AdminCustomers'),
            'search_link' => $this->context->link->getAdminLink('AdminPrivatePages') . '&ajax=1&action=getProducts',
            'activate_index' => AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . $token . '&tab_module=customers&activatecustomer=1&',
        ));
        return $this->display(__FILE__, 'views/templates/admin/form.tpl');
    }

    protected function readImageDir()
    {
        $dir = _PS_MODULE_DIR_ . 'privateshop/views/img/private';
        // Open a known directory, and proceed to read its contents
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                $images = array();
                while (($file = readdir($dh)) !== false) {
                    if (!is_dir($dir . $file) && $file != '.' && $file !== '..' && $file !== 'tmp' && $file !== 'index.php') {
                        $images[] = $file;
                    }
                }
                closedir($dh);
                return $images;
            }
        }
    }

    public function hookDisplayHeader()
    {
        /**
         * check for private shop
         */
        $this->checkPrivateShop();

        /**
         * check for password protected pages
         */
        $this->checkProtectedPage();
    }

    public function hookActionCustomerAccountAdd($params)
    {
        $this->disableNewCustomer($params['newCustomer']);
    }

    public function hookModuleRoutes()
    {
        return array(
            'module-privateshop-restricted' => array(
                'controller' => 'restricted',
                'rule' => 'restricted',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => $this->name,
                ),
            ),
            'module-privateshop-thejax' => array(
                'controller' => 'thejax',
                'rule' => 'private',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => $this->name,
                ),
            ),
            'module-privateshop-private' => array(
                'controller' => 'private',
                'rule' => 'private-access{/:type}',
                'keywords' => array(
                    'type' => array('regexp' => '[_a-zA-Z]+', 'param' => 'type'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => $this->name,
                ),
            ),
            'module-privateshop-protected' => array(
                'controller' => 'protected',
                'rule' => 'protected',
                'keywords' => array(
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => $this->name,
                ),
            ),
        );
    }

    protected function disableNewCustomer($customer)
    {
        if (true === (bool) Configuration::get('PRIVATE_SIGNUP_RESTRICT', false, $this->context->shop->id_shop_group, $this->context->shop->id)) {
            $customer->active = 0;
            $customer->update();
            //Send email
            $this->sendMailsUserPending($customer);
            $this->context->controller->errors[] = $this->translations['pending_validation'];
            $this->context->cookie->logout();
        }
    }

    private function sendMailsUserPending($customer)
    {
        //Send email to pending customer
        $module = new PrivateShop;
        $id_lang = (int) $this->context->language->id;
        $employee = new Employee(1);
        $admin_email = Configuration::get('PS_SHOP_EMAIL');
        $admin_email = (empty($admin_email)) ? $employee->email : $admin_email;
        $module->l('Account Pending Validation');
        $template_pending_customer = 'messageforpendingcustomer';
        $template_pending_customer_bo = 'messageforpendingcustomeradmin';
        $heading_pending_customer = Translate::getModuleTranslation('privateshop', 'Account Pending Validation', 'privateshop');
        Mail::Send(
            (int) $id_lang,
            $template_pending_customer,
            $heading_pending_customer,
            array('{name}' => $customer->firstname . ' ' . $customer->lastname),
            $customer->email,
            null,
            null,
            null,
            null,
            null,
            _PS_MODULE_DIR_ . 'privateshop/mails/',
            false,
            (int) $this->context->shop->id
        );
        //Send email to store Administrator
        Mail::Send(
            (int) $id_lang,
            $template_pending_customer_bo,
            $heading_pending_customer,
            array('{name}' => $customer->firstname . ' ' . $customer->lastname, '{email}' => $customer->email, '{id}' => $customer->id),
            $admin_email,
            null,
            null,
            null,
            null,
            null,
            _PS_MODULE_DIR_ . 'privateshop/mails/',
            false,
            (int) $this->context->shop->id
        );
    }

    private function sendMailCustomerAccount($customer)
    {
        //Send email to approved customer
        $module = new PrivateShop;
        $id_lang = (int) $this->context->language->id;
        $module->l('Account Approved');
        $template_pending_customer = 'messageforaccountactive';
        $heading_pending_customer = Translate::getModuleTranslation('privateshop', 'Account Approved', 'privateshop');
        $link_account = $this->context->link->getPageLink('my-account', true);
        Mail::Send(
            (int) $id_lang,
            $template_pending_customer,
            $heading_pending_customer,
            array('{name}' => $customer->firstname . ' ' . $customer->lastname, '{email}' => $customer->email, '{account}' => $link_account),
            $customer->email,
            null,
            null,
            null,
            null,
            null,
            _PS_MODULE_DIR_ . 'privateshop/mails/',
            false,
            (int) $this->context->shop->id
        );
    }

    /** Private functions */
    protected function checkPrivateShop()
    {
        $logged = false;
        if (isset($this->context->customer) && $this->context->customer->isLogged()) {
            $logged = true;
            $this->context->cookie->__set('access_granted', 1);
        } else {
            $logged = false;
            $this->context->cookie->__unset('access_granted');
        }

        $google_bot = (bool) Configuration::get('PRIVATE_RESTRICT_GOOGLE', false, $this->context->shop->id_shop_group, $this->context->shop->id);
        if ($google_bot && strstr(Tools::strtolower($_SERVER['HTTP_USER_AGENT']), 'googlebot')) {
            return;
        }

        $controller = Dispatcher::getInstance()->getController();
        $allowedControllers = Configuration::get('MODULE_PAGES', false, $this->context->shop->id_shop_group, $this->context->shop->id) && !empty(Configuration::get('MODULE_PAGES', false, $this->context->shop->id_shop_group, $this->context->shop->id)) ? explode(',', Configuration::get('MODULE_PAGES', false, $this->context->shop->id_shop_group, $this->context->shop->id)) : array();
        $modulePages = $this->getAvailableControllers(true);
        $filteredControllers = (isset($allowedControllers) && $allowedControllers && isset($modulePages) && $modulePages) ? array_intersect_key($modulePages, array_flip($allowedControllers)) : array();
        $filteredControllers = isset($filteredControllers) && $filteredControllers ? array_values($filteredControllers) : array();

        $whiteListControllers = array_merge(array('private', 'thejax', 'restricted', 'password', 'search'), $filteredControllers);
        if (in_array($controller, $whiteListControllers)) {
            return;
        }

        $ipList = array();
        $privateGroups = array();

        $ipAddress = Tools::getRemoteAddr();
        $privateType = Configuration::get('PRIVATIZE_SHOP', false, $this->context->shop->id_shop_group, $this->context->shop->id);
        $groupMgt = (bool) Configuration::get('PRIVATE_CUSTOMER_GROUP_STATE', false, $this->context->shop->id_shop_group, $this->context->shop->id);
        $currentUrl = Tools::getHttpHost(true) . $_SERVER['REQUEST_URI'];
        $whiteListUrl = (int) PrivateAcces::checkIfWhitelistedUrl($currentUrl);
        $id_customer = isset($this->context->customer) ? $this->context->customer->id : 0;
        $id_group = ($id_customer) ? (int) Customer::getDefaultGroupId((int) $id_customer) : (int) Group::getCurrent()->id;
        $params = array(
            'back' => Tools::htmlentitiesDecodeUTF8(urlencode($currentUrl)),
        );

        if (Configuration::get('PRIVATE_CUSTOMERS_GROUPS', false, $this->context->shop->id_shop_group, $this->context->shop->id)) {
            $privateGroups = explode(',', Configuration::get('PRIVATE_CUSTOMERS_GROUPS', false, $this->context->shop->id_shop_group, $this->context->shop->id));
        }

        if (!$whiteListUrl && (!isset($ipList) || (isset($ipList) && !in_array($ipAddress, $ipList)))) {
            switch ($privateType) {
                case 'whole-shop':
                    if (!$id_customer || !$logged) {
                        $params['type'] = 'private_login';
                        Tools::redirect($this->context->link->getModuleLink(
                            $this->name,
                            'private',
                            $params,
                            true,
                            $this->context->language->id,
                            $this->context->shop->id
                        ));
                    }
                    break;
                case 'selected-parts':
                    if (!$id_customer || !$logged) {
                        $this->runPrivateShop();
                    } else {
                        if (true === $groupMgt && isset($privateGroups) && !in_array($id_group, $privateGroups)) {
                            $this->runPrivateShop('deadend');
                        }
                    }
                    break;
            }
        }
    }

    protected function runPrivateShop($type = 'private_login')
    {
        $privateCms = array();
        $privateProduct = array();
        $privateCategory = array();

        $controller = Dispatcher::getInstance()->getController();
        $currentUrl = Tools::getHttpHost(true) . $_SERVER['REQUEST_URI'];
        $blackListUrl = (int) PrivateAcces::checkIfBlacklistedUrl($currentUrl);

        if (Configuration::get('categoryBox', false, $this->context->shop->id_shop_group, $this->context->shop->id)) {
            $privateCategory = explode(',', Configuration::get('categoryBox', false, $this->context->shop->id_shop_group, $this->context->shop->id));
        }
        if (Configuration::get('private_products', false, $this->context->shop->id_shop_group, $this->context->shop->id)) {
            $privateProduct = explode(',', Configuration::get('private_products', false, $this->context->shop->id_shop_group, $this->context->shop->id));
        }
        if (Configuration::get('cms_pages', false, $this->context->shop->id_shop_group, $this->context->shop->id)) {
            $privateCms = explode(',', Configuration::get('cms_pages', false, $this->context->shop->id_shop_group, $this->context->shop->id));
        }

        $params = array(
            'type' => $type,
        );

        if ($type != 'deadend') {
            $params['back'] = Tools::htmlentitiesDecodeUTF8(urlencode($currentUrl));
        }

        $identifier = false;
        $privateParent = false;
        $identifierResource = array();
        if (in_array($controller, array('product', 'category', 'cms', 'index'))) {
            $identifier = Tools::getValue('id_' . $controller);
            if ('index' === $controller) {
                $identifier = Configuration::get('PS_HOME_CATEGORY');
                $identifierResource = $privateCategory;
                //$back = $this->context->link->getBaseLink($this->context->shop->id, true);
            } else {
                $identifierResource = ${'private' . Tools::ucfirst($controller)};
            }

            if ('category' === $controller) {
                $currentCategory = new Category((int) $identifier, $this->context->language->id, $this->context->shop->id);
                $parents = $currentCategory->getParentsCategories($this->context->language->id);
                foreach ($parents as $parent) {
                    if (isset($privateCategory) && in_array($parent['id_category'], $privateCategory)) {
                        $privateParent = true;
                        break;
                    }
                }
            }
        }

        if ($blackListUrl || $privateParent || ($identifier && isset($identifierResource) && in_array($identifier, $identifierResource))) {
            Tools::redirect($this->context->link->getModuleLink(
                $this->name,
                'private',
                $params,
                true,
                $this->context->language->id,
                $this->context->shop->id
            ));
        }
    }

    protected function checkProtectedPage()
    {
        $privatePages = array();
        $controller = Dispatcher::getInstance()->getController();
        if (!empty($controller) && $controller) {
            $value = false;
            if (in_array($controller, array('product', 'category', 'cms'))) {
                $value = (int)Tools::getValue('id_'.$controller);
            } else {
                $page = $controller;
                if (isset($this->context->controller->php_self) && $this->context->controller->php_self) {
                    $page = $this->context->controller->php_self;
                }
                $metaData = Meta::getMetaByPage($page, $this->context->language->id);
                if (isset($metaData) && $metaData) {
                    $controller = 'meta_pages';
                    $value = (int)$metaData['id_meta'];
                }
            }
            $privatePages = PrivatePages::checkPage($controller, $value);
        }

        //protect product if default category is restricted
        if ((!isset($privatePages) || $privatePages == null) && (!empty($controller) && $controller === 'product')) {
            if (Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product')))) {
                $privatePages = PrivatePages::checkPage('category', $product->id_category_default);
            }
        } elseif ((!isset($privatePages) || $privatePages == null) && (!empty($controller) && $controller === 'category')) {
            $currentCategory = new Category(
                (int) Tools::getValue('id_category'),
                $this->context->language->id,
                $this->context->shop->id
            );
            $parents = $currentCategory->getParentsCategories($this->context->language->id);
            foreach ($parents as $parent) {
                if (($privatePages = PrivatePages::checkPage('category', $parent['id_category']))) {
                    break;
                }
            }
        }

        if (isset($privatePages) && $privatePages) {
            $hash = md5($privatePages['id_privateshop_protecetd_pages']);
            if (!$this->context->cookie->__isset($hash) ||
            ($this->context->cookie->__isset($hash) && false === $this->context->cookie->__get($hash))) {
                $this->context->cookie->__set('_privte_block_', (int)$privatePages['id_privateshop_protecetd_pages']);

                $currentUrl = Tools::getHttpHost(true) . $_SERVER['REQUEST_URI'];
                Tools::redirect($this->context->link->getModuleLink(
                    $this->name,
                    'protected',
                    array('back' => Tools::htmlentitiesDecodeUTF8(urlencode($currentUrl))),
                    true,
                    $this->context->language->id,
                    $this->context->shop->id
                ));
            }
        }
    }

    public function initPrivate()
    {
        $use_ssl = ((isset($this->context->controller->ssl) && $this->context->controller->ssl && Configuration::get('PS_SSL_ENABLED')) || Tools::usingSecureMode()) ? true : false;
        $protocol_content = ($use_ssl) ? 'https://' : 'http://';
        $this->context->smarty->assign(Meta::getMetaTags($this->context->language->id, 'private_login'));
        $this->assignDate();

        $jQueryPath = false;
        if (Tools::version_compare(_PS_VERSION_, '1.7.0.0', '<=') == true) {
            $jQueryPath = Media::getJqueryPath(_PS_JQUERY_VERSION_);
            if (is_array($jQueryPath) && isset($jQueryPath[0])) {
                $jQueryPath = $jQueryPath[0];
            }
        }
        $this->context->smarty->assign(array('jQuery_path' => $jQueryPath));
        $this->context->smarty->assign(array(
            'modules_dir' => _MODULE_DIR_,
            'js_dir' => _THEME_JS_DIR_,
            'js_def' => Media::getJsDef(),
            'version' => _PS_VERSION_,
            'css_dir' => _THEME_CSS_DIR_,
            'tpl_dir' => _PS_THEME_DIR_,
            'token' => Tools::getToken(),
            'static_token' => Tools::getToken(false),
            'js_files' => Media::getJqueryPath(),
            'errors' => $this->context->controller->errors,
            'shop_name' => Configuration::get('PS_SHOP_NAME'),
            'content_only' => (int)Tools::getValue('content_only'),
            'priceDisplayPrecision' => _PS_PRICE_DISPLAY_PRECISION_,
            'is_guest' => (bool) $this->context->customer->isGuest(),
            'is_logged' => (bool) $this->context->customer->isLogged(),
            'date_format' => $this->context->language->date_format_lite,
            'favicon_url' => _PS_IMG_ . Configuration::get('PS_FAVICON'),
            'img_update_time' => Configuration::get('PS_IMG_UPDATE_TIME'),
            'request_uri' => Tools::safeOutput(urldecode($_SERVER['REQUEST_URI'])),
            'countries' => Country::getCountries($this->context->language->id, true),
            'logo_url' => $this->context->link->getMediaLink(_PS_IMG_ . Configuration::get('PS_LOGO')),
            'language_code' => $this->context->language->language_code ? $this->context->language->language_code : $this->context->language->iso_code,
            'base_uri' => $protocol_content . Tools::getHttpHost() . __PS_BASE_URI__ . (!Configuration::get('PS_REWRITING_SETTINGS') ? 'index.php' : ''),
        ));
    }

    public function assignDate()
    {
        $selectedYears = (int) (Tools::getValue('years', 0));
        $years = Tools::dateYears();
        $selectedMonths = (int) (Tools::getValue('months', 0));
        $months = Tools::dateMonths();
        $selectedDays = (int) (Tools::getValue('days', 0));
        $days = Tools::dateDays();

        $this->context->smarty->assign(array(
            'years' => $years,
            'sl_year' => $selectedYears,
            'months' => $months,
            'sl_month' => $selectedMonths,
            'days' => $days,
            'sl_day' => $selectedDays,
        ));
    }

    /**
     * Assign countries var to smarty
     */
    public function assignCountries()
    {
        $id_country = (int)Tools::getCountry();
        if (Configuration::get('PS_RESTRICT_DELIVERED_COUNTRIES')) {
            $countries = Carrier::getDeliveredCountries($this->context->language->id, true, true);
        } else {
            $countries = Country::getCountries($this->context->language->id, true);
        }
        $this->context->smarty->assign(array(
            'countries' => $countries,
            'PS_REGISTRATION_PROCESS_TYPE' => Configuration::get('PS_REGISTRATION_PROCESS_TYPE'),
            'sl_country' => (int)$id_country,
            'vat_management' => Configuration::get('VATNUMBER_MANAGEMENT')
        ));
    }

    public function assignAddressFormat()
    {
        $id_country = (int)Tools::getCountry();
        $addressItems = array();
        $addressFormat = AddressFormat::getOrderedAddressFields((int)$id_country, false, true);
        $requireFormFieldsList = AddressFormat::getFieldsRequired();

        foreach ($addressFormat as $addressline) {
            foreach (explode(' ', $addressline) as $addressItem) {
                $addressItems[] = trim($addressItem);
            }
        }

        // Add missing require fields for a new user susbscription form
        foreach ($requireFormFieldsList as $fieldName) {
            if (!in_array($fieldName, $addressItems)) {
                $addressItems[] = trim($fieldName);
            }
        }

        foreach (array('inv', 'dlv') as $addressType) {
            $this->context->smarty->assign(array(
                $addressType.'_adr_fields' => $addressFormat,
                $addressType.'_all_fields' => $addressItems,
                'required_fields' => $requireFormFieldsList
            ));
        }
    }

    public function getPrivateConfigurationValues()
    {
        $bg_video = rtrim(Configuration::get('BACKGROUND_VIDEO', false, $this->context->shop->id_shop_group, $this->context->shop->id), '/');
        $bg_video = explode('/', $bg_video);
        $bg_video = end($bg_video);
        $bg_opacity = empty(Configuration::get('BG_OPACITY', false, $this->context->shop->id_shop_group, $this->context->shop->id)) ? 1 : Configuration::get('BG_OPACITY', false, $this->context->shop->id_shop_group, $this->context->shop->id);
        $theme = (empty(Configuration::get('PRIVATE_FORM_THEME', false, $this->context->shop->id_shop_group, $this->context->shop->id))) ? 'mod' : Configuration::get('PRIVATE_FORM_THEME', false, $this->context->shop->id_shop_group, $this->context->shop->id);
        $conf_values = array(
            'bg_video' => $bg_video,
            'bg_opacity' => $bg_opacity,
            'priv_form_theme' => $theme,
            'bg_img' => Configuration::get('bg_image', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'pages' => Configuration::get('cms_pages', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'bday' => Configuration::get('PRIVATE_BDAY', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'category' => Configuration::get('categoryBox', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'position' => Configuration::get('FORM_POSITION', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'bg_type' => Configuration::get('BACKGROUND_TYPE', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'bg_color' => Configuration::get('BACKGROUND_COLOR', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'products' => Configuration::get('private_products', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'allowed_ip' => Configuration::get('ACCESS_GRANTED_IP', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'active_signup' => Configuration::get('PRIVATE_SIGNUP', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'PRIVATIZE_SHOP' => Configuration::get('PRIVATIZE_SHOP', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'gender_opt' => Configuration::get('PRIVATE_GENDER_OPT', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'nletter_opt' => Configuration::get('PRIVATE_NLETTER_OPT', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'offers_opt' => Configuration::get('PRIVATE_OFFERS_OPT', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'custom_logo' => Configuration::get('PRIVATE_CUSTOM_LOGO', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'bg_video_img' => Configuration::get('BACKGROUND_VIDEO_IMG', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'custom_logo_img' => Configuration::get('PRIVATE_CUSTOM_LOGO_IMG', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'show_store_title' => (int) Configuration::get('PRIVATE_SHOW_STORE_TITLE', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'show_header_footer' => (int) Configuration::get('PRIVATE_SHOW_HEADER_FOOTER', false, $this->context->shop->id_shop_group, $this->context->shop->id),
            'login_title' => Configuration::get('LOGIN_TITLE', (int) $this->context->language->id, $this->context->shop->id_shop_group, $this->context->shop->id),
            'signup_title' => Configuration::get('SIGNUP_TITLE', (int) $this->context->language->id, $this->context->shop->id_shop_group, $this->context->shop->id),
            'restrict_message' => Configuration::get('PRIVATE_CUSTOMER_GROUP_MSG', (int) $this->context->language->id, $this->context->shop->id_shop_group, $this->context->shop->id),
        );
        return $conf_values;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderPasswordPageForm()
    {
        $helper = new HelperForm();
        $helper->id = (int) Tools::getValue('id_privateshop_protecetd_pages');
        $helper->show_toolbar = false;
        $helper->show_cancel_button = false;
        $helper->table = 'privateshop_protecetd_pages';
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = 'id_privateshop_protecetd_pages';
        $helper->submit_action = 'submitPasswdPages';
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->back_url = $helper->currentIndex . '&token=' . $helper->token;
        $products = PrivatePages::getPrivateProducts(Tools::getValue('id_privateshop_protecetd_pages'), $this->context->language->id);

        if ($post_accessories = Tools::getValue('inputPrivateProducts')) {
            $post_accessories_tab = explode('-', $post_accessories);
            foreach ($post_accessories_tab as $id_product) {
                if (!$this->hasProduct($id_product, $products) &&
                    $product = PrivatePages::getProductById($id_product)) {
                    $products[] = $product;
                }
            }
        }

        $helper->tpl_vars = array(
            'fields_value' => $this->getPrivatePageFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'products' => $products,
            'context_link' => Context::getContext()->link,
        );

        return $helper->generateForm(array($this->getPrivatePageForm()));
    }

    /**
     * Generate password protected page form
     * @return array
     */
    protected function getPrivatePageForm()
    {
        $pages = PrivateAcces::getAllMeta($this->context->language->id);
        $exludePages = array(
            'index',
            'category',
            'changecurrency',
            'cms',
            'footer',
            'header',
            'pagination',
            'product',
            'product-sort',
            'statistics',
        );

        foreach ($pages as $key => $page) {
            if (in_array($page['page'], $exludePages) || preg_match('#module-([a-z0-9_-]+)-([a-z0-9]+)$#i', $page['page'])) {
                unset($pages[$key]);
            }
        }

        $radio = (true === Tools::version_compare(_PS_VERSION_, '1.6.0.0', '>=')) ? 'switch' : 'radio';
        $pageType = array(
            array('id' => 'product', 'name' => $this->getTypes('product')),
            array('id' => 'category', 'name' => $this->getTypes('category')),
            array('id' => 'cms', 'name' => $this->getTypes('cms')),
            array('id' => 'meta_pages', 'name' => $this->getTypes('meta_pages')),
        );
        $selected_categories = (Tools::getValue('id_privateshop_protecetd_pages')) ? PrivatePages::getValueById(Tools::getValue('id_privateshop_protecetd_pages')) : array();

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'hidden',
                        'name' => 'id_privateshop_protecetd_pages',
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'currentFormTab',
                    ),
                    array(
                        'type' => $radio,
                        'label' => $this->l('Active'),
                        'name' => 'active',
                        'is_bool' => true,
                        'desc' => $this->l('Enable/Disable password protection.'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled'),
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled'),
                            ),
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Page'),
                        'name' => 'type',
                        'required' => true,
                        'options' => array(
                            'query' => $pageType,
                            'id' => 'id',
                            'name' => 'name',
                        ),
                        'col' => 2,
                    ),
                    array(
                        'type' => 'product',
                        'label' => $this->l('Search Product'),
                        'name' => 'product',
                        'col' => 6,
                        'prefix' => '<i class="icon-search"></i>',
                        'hint' => $this->l('Seleced product(s) will be password protected.'),
                    ),
                    array(
                        'type' => 'categories',
                        'label' => $this->l('Categories'),
                        'name' => 'category',
                        'col' => 6,
                        'tree' => array(
                            'id' => 'type_category',
                            'use_checkbox' => true,
                            'disabled_categories' => null,
                            'selected_categories' => $selected_categories,
                            'root_category' => Context::getContext()->shop->getCategory(),
                        ),
                    ),
                    array(
                        'type' => 'cms_pages',
                        'label' => $this->l('CMS pages'),
                        'name' => 'cms[]',
                        'values' => CMS::getLinks((int) $this->context->language->id),
                        'desc' => $this->l('Please mark every page that you want to password protect.'),
                    ),
                    array(
                        'type' => 'meta_pages',
                        'label' => $this->l('Meta Pages'),
                        'name' => 'meta_pages[]',
                        'values' => $pages,
                        'desc' => $this->l('Please mark every meta page that you want to password protect.'),
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Message'),
                        'name' => 'message',
                        'autoload_rte' => true,
                        'lang' => true,
                        'col' => 8,
                        'hint' => $this->l('Invalid characters:') . ' <>;=#{}',
                    ),
                    array(
                        'type' => 'pwd',
                        'label' => $this->l('Password'),
                        'name' => 'passwd',
                        'col' => 6
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    'cancel' => array(
                        'title' => $this->l('Cancel'),
                        'class' => 'btn btn-default',
                        'icon' => 'process-icon-cancel',
                        'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                    ),
                ),
            ),
        );

        if (Shop::isFeatureActive()) {
            $fields_form['form']['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'name' => 'checkBoxShopAsso',
            );
        }

        return $fields_form;
    }

    /**
     * Set values for the inputs.
     * @return array
     */
    protected function getPrivatePageFormValues()
    {
        $fields_value = array();
        $fields_value = array(
            'shop' => array(),
            'currentFormTab' => 'passwordpage',
            'message' => Tools::getValue('message')
        );

        if (Tools::getValue('id_privateshop_protecetd_pages') && (Tools::isSubmit('submitPasswdPages') === false)) {
            $pwdBlock = new PrivatePages((int) Tools::getValue('id_privateshop_protecetd_pages'));
            $fields_value['id_privateshop_protecetd_pages'] = $pwdBlock->id;
            $fields_value['active'] = $pwdBlock->active;
            $fields_value['date_add'] = $pwdBlock->date_add;
            $fields_value['message'] = $pwdBlock->message;
            $fields_value['passwd'] = $pwdBlock->passwd;
            $fields_value['type'] = PrivatePages::getPrivateType($pwdBlock->id);
            $fields_value[$fields_value['type']] = PrivatePages::getValueById($pwdBlock->id);
        } else {
            $fields_value['id_privateshop_protecetd_pages'] = Tools::getValue('id_privateshop_protecetd_pages');
            $fields_value['active'] = (int) Tools::getValue('active');
            $fields_value['passwd'] = Tools::getValue('passwd');
            $fields_value['type'] = Tools::getValue('type', 'product');
            $fields_value[$fields_value['type']] = Tools::getValue($fields_value['type']);
            $fields_value['date_add'] = date('Y-m-d H:i:s');

            /* getting multilingual fields*/
            $languages = Language::getLanguages(false);
            foreach ($languages as $lang) {
                $message = Tools::getValue('message_' . $lang['id_lang']);
                // getting message
                if (!empty($message) && !Validate::isCleanHtml($message)) {
                    $this->context->controller->errors[] = sprintf(
                        $this->l('Invalid message content in %s.'),
                        Language::getIsoById($lang['id_lang'])
                    );
                } else {
                    $fields_value['message'][(int) $lang['id_lang']] = $message;
                }
            }
        }
        return $fields_value;
    }

    /**
     * generate password protected page lsit
     * @return array
     */
    protected function renderPasswordProtectedPagesList()
    {
        $fields_list = array(
            'id_privateshop_protecetd_pages' => array(
                'title' => $this->l('ID'),
                'width' => 120,
                'type' => 'text',
                'search' => false,
                'orderby' => false,
            ),
            'type' => array(
                'title' => $this->l('Type'),
                'width' => 140,
                'type' => 'text',
                'callback' => 'getTypes',
                'callback_object' => $this,
                'search' => false,
                'orderby' => false,
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'width' => 140,
                'active' => 'status',
                'type' => 'bool',
                'search' => false,
                'orderby' => false,
            ),
            'date_add' => array(
                'title' => $this->l('Created Date'),
                'width' => 140,
                'type' => 'datetime',
                'search' => false,
                'orderby' => false,
            ),
        );

        // multishop feature
        // if (Shop::isFeatureActive()) {
        //     $fields_list['id_shop'] = array(
        //         'title' => $this->l('Shop'),
        //         'align' => 'center',
        //         'width' => 25,
        //         'type' => 'int',
        //     );
        // }

        $listContent = PrivatePages::getListContent();
        $helper = new HelperList();
        $helper->listTotal = count($listContent);
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_privateshop_protecetd_pages';
        $helper->actions = array('edit', 'delete');
        $helper->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?'),
            ),
        );
        $helper->show_toolbar = true;
        $helper->title = $this->l('Password Protected Pages');
        $helper->table = 'privateshop_protecetd_pages';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->toolbar_btn['new'] = array(
            'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&add' . $helper->table . '&token=' . Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new'),
        );
        return $helper->generateList($listContent, $fields_list);
    }

    /**
     * return translatable resource type name
     * @param  string $type
     * @return string
     */
    public function getTypes($type)
    {
        switch ($type) {
            case 'product':
                return $this->l('Product');
            case 'category':
                return $this->l('Category');
            case 'cms':
                return $this->l('CMS');
            case 'meta_pages':
                return $this->l('Meta Pages');
        }
    }

    /**
     * check if product exist in pool of products
     * @param  int  $id_product
     * @param  array  $products  
     * @return boolean            
     */
    public function hasProduct($id_product, $products)
    {
        foreach ($products as $product) {
            if ((int) $product['id_product'] == (int) $id_product) {
                return true;
            }
        }
        return false;
    }

    /**
     * add ajax tab
     * @param  string $controllerClassName
     * @param  string $tabName            
     * @param  string $icon               
     * @return bool
     */
    public function installTab($controllerClassName, $tabName, $icon = null)
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->id_parent = -1;
        $tab->module = $this->name;
        $tab->class_name = $controllerClassName;
        $tab->name = array();

        if (true === Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=') && !empty($icon)) {
            $tab->icon = $icon;
        }

        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $tabName;
        }

        return (bool) $tab->add();
    }

    /**
     * remomve Admin ajax tab
     * @return bool
     */
    public function uninstallTab()
    {
        $res = true;
        if (Validate::isLoadedObject($tab = Tab::getInstanceFromClassName('AdminPrivatePages'))) {
            $res &= $tab->delete();
        }
        return $res;
    }

    /**
     * handle password protected page submission
     * @return array
     */
    protected function submitPrivatePage()
    {
        $form_values = $this->getPrivatePageFormValues();

        if (!isset($form_values) && !$form_values) {
            return $this->context->controller->errors[] = $this->l('Empty post values.');
        }

        if (!isset($form_values[$form_values['type']]) ||
            !$form_values[$form_values['type']] ||
            ($form_values[$form_values['type']] && !count($form_values[$form_values['type']]))) {
            $this->context->controller->errors[] = sprintf($this->l('You must select at least one %s.'), $form_values['type']);
        }
        if (isset($form_values['passwd']) && !Validate::isPasswd($form_values['passwd'])) {
            $this->context->controller->errors[] = $this->l('Password field is invalid.');
        }

        if (isset($form_values['message']) && $form_values['message']) {
            foreach ($form_values['message'] as $id_lang => $message) {
                if (isset($message) && $message && !Validate::isCleanHtml($message)) {
                    $this->context->controller->errors[] = sprintf(
                        $this->l('Invalid value message content in %s.'),
                        Language::getIsoById($id_lang)
                    );
                }
            }
        }

        if (count($this->context->controller->errors)) {
            return $this->context->controller->errors;
        }

        $fn = 'add';
        Shop::addTableAssociation('privateshop_protecetd_pages', array('type' => 'shop'));
        if (($id_privateshop_protecetd_pages = Tools::getValue('id_privateshop_protecetd_pages'))) {
            $fn = 'update';
            $privatePage = new PrivatePages((int) $id_privateshop_protecetd_pages);
        } else {
            $privatePage = new PrivatePages();
        }

        $privatePage->active = $form_values['active'];
        $privatePage->passwd = $form_values['passwd'];
        $privatePage->message = $form_values['message'];

        if (!$privatePage->save()) {
            $this->context->controller->errors = sprintf($this->l('Something went wrong while performing operation %s'), $fn);
        } else {
            $privatePage->deletePrivatePage();
            $conditions = array();
            foreach ($form_values[$form_values['type']] as $value) {
                $conditions[] = array(
                    'id_privateshop_protecetd_pages' => (int) $privatePage->id,
                    'type' => pSQL($form_values['type']),
                    'value' => pSQL($value),
                );
            }
            $privatePage->addPrivatePage($conditions);
            $this->context->controller->confirmations[] = sprintf($this->l('Settings %sed successfully.'), $fn);
        }
    }

    /**
     * Update context after customer login.
     *
     * @param Customer $customer Created customer
     */
    public function updateCustomer(Customer $customer)
    {
        $this->context->customer = $customer;
        $this->context->cookie->id_customer = (int) $customer->id;
        $this->context->cookie->customer_lastname = $customer->lastname;
        $this->context->cookie->customer_firstname = $customer->firstname;
        $this->context->cookie->passwd = $customer->passwd;
        $this->context->cookie->logged = 1;
        $customer->logged = 1;
        $this->context->cookie->email = $customer->email;
        $this->context->cookie->is_guest = $customer->isGuest();
        $this->context->cart->secure_key = $customer->secure_key;

        if (Configuration::get('PS_CART_FOLLOWING') && (empty($this->context->cookie->id_cart) || Cart::getNbProducts($this->context->cookie->id_cart) == 0) && $idCart = (int) Cart::lastNoneOrderedCart($this->context->customer->id)) {
            $this->context->cart = new Cart($idCart);
        } else {
            $idCarrier = (int) $this->context->cart->id_carrier;
            $this->context->cart->id_carrier = 0;
            $this->context->cart->setDeliveryOption(null);
            $this->context->cart->updateAddressId($this->context->cart->id_address_delivery, (int) Address::getFirstCustomerAddressId((int) ($customer->id)));
            $this->context->cart->id_address_delivery = (int) Address::getFirstCustomerAddressId((int) ($customer->id));
            $this->context->cart->id_address_invoice = (int) Address::getFirstCustomerAddressId((int) ($customer->id));
        }
        $this->context->cart->id_customer = (int) $customer->id;

        if (isset($idCarrier) && $idCarrier) {
            $deliveryOption = [$this->context->cart->id_address_delivery => $idCarrier . ','];
            $this->context->cart->setDeliveryOption($deliveryOption);
        }

        $this->context->cart->save();
        $this->context->cookie->id_cart = (int) $this->context->cart->id;
        $this->context->cookie->write();
        $this->context->cart->autosetProductAddress();
    }

    /**
     * setting id_shop and id_shop_group
     */
    public function setShopIdx()
    {
        if ($this->id_shop === null || !Shop::isFeatureActive()) {
            $this->id_shop = Shop::getContextShopID();
        } else {
            $this->id_shop = Context::getContext()->shop->id;
        }
        if ($this->id_shop_group === null || !Shop::isFeatureActive()) {
            $this->id_shop_group = Shop::getContextShopGroupID();
        } else {
            $this->id_shop_group = Context::getContext()->shop->id_shop_group;
        }
    }

    /**
     * upgrade function to generate missing tables
     * @return bool
     */
    public function generatePrivatePageTables()
    {
        return PrivatePages::generateTables();
    }

    /**
     * clear cache
     * @return void
     */
    public function removeCache()
    {
        Tools::clearSmartyCache();
        Tools::clearXMLCache();
        Media::clearCache();
        Tools::generateIndex();
        if (true === Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=') &&
            is_callable(array('Tools', 'clearAllCache'))) {
            Tools::clearAllCache();
        }
    }

    /**
     * get shop translations
     * @return array
     */
    protected function getPrivateShopTranslations()
    {
        return array(
            'welcome' => $this->l('Welcome!'),
            'email_required' => $this->l('An email address is required.'),
            'passwd_required' => $this->l('Password is required.'),
            'invalid_email' => $this->l('Invalid email address.'),
            'invalid_password' => $this->l('Invalid password.'),
            'required_firstname' => $this->l('First name is required.'),
            'invalid_firstname' => $this->l('First name is invalid.'),
            'required_lastname' => $this->l('Last name is required.'),
            'invalid_lastname' => $this->l('Last name is invalid.'),
            'invalid_birthday' => $this->l('Birth date is invalid.'),
            'account_deactive' => $this->l('Your account isn\'t available at this time, please contact us'),
            'auth_error' => $this->l('Authentication failed.'),
            'guest_account_error' => $this->l('You cannot create a guest account.'),
            'duplicate_email_error' => $this->l('An account using this email address has already been registered.'),
            'phone_error' => $this->l('You must register at least one phone number.'),
            'account_creation_error' => $this->l('An error occurred while creating your account.'),
            'country_error' => $this->l('Country cannot be loaded with address->id_country'),
            'country_deactive' => $this->l('This country is not active.'),
            'zipcode_required' => $this->l('A Zip / Postal code is required.'),
            'zicode_invalid' => $this->l('The Zip / Postal code is invalid.'),
            'invalid_zipcode' => $this->l('The Zip/Postal code you\'ve entered is invalid. It must follow this format: %s'),
            'identificaion_invalid' => $this->l('The identification number is incorrect or has already been used.'),
            'invalid_country' => $this->l('Country is invalid'),
            'state_required' => $this->l('This country requires you to choose a State.'),
            'address_error' => $this->l('An error occurred while creating your address.'),
            'email_sending_error' => $this->l('The email cannot be sent.'),
            'pending_validation' => $this->l('Please have patience. Your Account is pending for validation.'),
            'no_account_registered' => $this->l('There is no account registered for this email address.'),
            'cannot_regen_pwd' => $this->l('You cannot regenerate the password for this account.'),
            'gen_pwd_after_x' => $this->l('You can regenerate your password only every %d minute(s)'),
            'email_sending_error' => $this->l('An error occurred while sending the email.'),
            'account_404' => $this->l('Customer account not found'),
            'pwd_sending_failed' => $this->l('An error occurred with your account, which prevents us from sending you a new password. Please report this issue using the contact form.'),
            'invalid_pwd_data' => $this->l('We cannot regenerate your password with the data you\'ve submitted.'),
            'account_exists' => $this->l('An account using this email address has already been registered. Please enter a valid password or request a new one.'),
        );
    }
}
