<?php
/**
 * PrivateShop
 *
 * Do not edit or add to this file.
 * You are not authorized to modify, copy or redistribute this file.
 * Permissions are reserved by FME Modules.
 *
 *  @author    FME Modules
 *  @copyright 2020 FME Modules All right reserved
 *  @license   FME Modules
 *  @category  FMM Modules
 *  @package   PrivateShop
 */

class PrivateShopProtectedModuleFrontController extends ModuleFrontController
{

    public $ssl = true;

    protected $redirect_url = '';

    protected $privatePage = null;

    protected $tpl = 'protected-page_16.tpl';

    public function init()
    {
        parent::init();
        $this->context = Context::getContext();
        
        if ((true) === Tools::version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            $this->display_column_left = false;
            $this->display_column_right = false;
        }

        $home = $this->context->link->getBaseLink($this->context->shop->id, true);
        $this->redirect_url = Tools::htmlentitiesDecodeUTF8(urldecode(Tools::getValue('back')));
        $id_protected_page = ($this->context->cookie->__isset('_privte_block_'))?(int)$this->context->cookie->__get('_privte_block_'):0;

        if (!$id_protected_page || !Validate::isLoadedObject($this->privatePage = new PrivatePages((int)$id_protected_page, $this->context->language->id))) {
            Tools::redirect('index.php?controller=pagenotfound');
        }

        if (true === (bool) Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $this->tpl = 'module:'.$this->module->name.'/views/templates/front/protected-page_17.tpl';
        }

        if ($this->context->cookie->__isset(md5($id_protected_page)) && $this->context->cookie->__get(md5($id_protected_page))) {
            $this->redirect_url = (isset($this->redirect_url) && $this->redirect_url)? $this->redirect_url : $home;
            Tools::redirect($this->redirect_url);
        }
    }

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
        if (null === $this->privatePage || !$this->privatePage->active) {
            Tools::redirect('index.php?controller=pagenotfound');
        } else {
            $this->context->smarty->assign(array(
                'ps_version' => _PS_VERSION_,
                'message' => $this->privatePage->message,
            ));
            $this->setTemplate($this->tpl);
        }
    }

    public function displayAjaxProtectedAccess()
    {
        $result = array('errors' => null, 'success' => false, 'redirect_url' =>  false);
        $passwd = trim(Tools::getValue('passwd'));

        if (empty($passwd) || $passwd !== $this->privatePage->passwd) {
            $result['errors'] = $this->module->translations['invalid_password'];
        } else {
            $result['success'] = true;
            $result['redirect_url'] = true;
            $this->context->cookie->__set(md5($this->privatePage->id), true);
        }
        die(Tools::jsonEncode($result));
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->addJS($this->module->getLocalPath() . 'views/js/protected-page.js');
    }
}
