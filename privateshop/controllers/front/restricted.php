<?php
/**
 * PrivateShop
 *
 * Do not edit or add to this file.
 * You are not authorized to modify, copy or redistribute this file.
 * Permissions are reserved by FME Modules.
 *
 *  @author    FME Modules
 *  @copyright 2020 FME Modules All right reserved
 *  @license   Copyrights FME Modules
 *  @category  FMM Modules
 *  @package   PrivateShop
 */

class PrivateShopRestrictedModuleFrontController extends ModuleFrontController
{
    public $ssl = true;

    public function init()
    {
        parent::init();
        $this->context = Context::getContext();
        if (Tools::version_compare(_PS_VERSION_, '1.7.0.0', '<') == true) {
            $this->display_header = false;
            $this->display_footer = false;
        }
        
    }
    
    public function initContent()
    {
        parent::initContent();
        
        $use_ssl = ((isset($this->ssl) && $this->ssl && Configuration::get('PS_SSL_ENABLED')) || Tools::usingSecureMode()) ? true : false;
        $protocol_content = ($use_ssl) ? 'https://' : 'http://';
        $metas = Meta::getMetaByPage('module-privateshop-restricted', $this->context->language->id);
        $field_values = $this->module->getPrivateConfigurationValues();
        $version = (Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=') == true) ? 1 : 0;
        $this->context->smarty->assign(array(
            'meta_title' => $metas['title'],
            'meta_description' => $metas['description'],
            'meta_keywords' => $metas['keywords'],
            'field_values' => $field_values,
            'version' => (int)$version,
            'modules_dir' => _MODULE_DIR_,
            'css_dir' => _THEME_CSS_DIR_,
            'favicon_url' => _PS_IMG_.Configuration::get('PS_FAVICON'),
            'img_update_time' => Configuration::get('PS_IMG_UPDATE_TIME'),
            'shop_name' => Configuration::get('PS_SHOP_NAME'),
            'request_uri' => Tools::safeOutput(urldecode($_SERVER['REQUEST_URI'])),
            'logo_url' => $this->context->link->getMediaLink(_PS_IMG_.Configuration::get('PS_LOGO')),
            'language_code' => $this->context->language->language_code ? $this->context->language->language_code : $this->context->language->iso_code,
            'base_uri' => $protocol_content.Tools::getHttpHost().__PS_BASE_URI__.(!Configuration::get('PS_REWRITING_SETTINGS') ? 'index.php' : ''),
        ));

        if (Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=') == true) {
            $this->setTemplate('module:privateshop/views/templates/front/restricted.tpl');
        } else {
            $this->setTemplate('restricted.tpl');
        }
    }
}