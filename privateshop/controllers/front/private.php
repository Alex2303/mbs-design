<?php
/**
* PrivateShop
*
* Do not edit or add to this file.
* You are not authorized to modify, copy or redistribute this file.
* Permissions are reserved by FME Modules.
*
*  @author    FME Modules
*  @copyright 2020 FME Modules All right reserved
*  @license   FME Modules
*  @category  FMM Modules
*  @package   PrivateShop
*/

class PrivateShopPrivateModuleFrontController extends ModuleFrontController
{
    protected $persist = false;

    protected $header_footer = false;

    protected $privateTemplate;

    protected $accessType = 'private_login';

    public function init()
    {
        //phpinfo();
        parent::init();
        $this->context = Context::getContext();
        $this->accessType = Tools::getValue('type');
        $this->persist = (int) Tools::getValue('persist', 0);
        $this->header_footer = (bool) Configuration::get('PRIVATE_SHOW_HEADER_FOOTER', false, $this->context->shop->id_shop_group, $this->context->shop->id);
        
        if ((true) === Tools::version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            $this->display_column_left = false;
            $this->display_column_right = false;
        }

        if ($this->header_footer) {
            $this->privateTemplate = 'private-shop-header-footer.tpl';
            if (true === (bool) Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->privateTemplate = 'module:privateshop/views/templates/front/private-shop-header-footer_17.tpl';
            } else {
                $this->display_header = true;
                $this->display_footer = true;
            }
        } else {
            $this->privateTemplate = $this->accessType . '.tpl';
            if (true === (bool) Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $suffix = ('private_login' == $this->accessType) ? '_17' : '';
                $this->privateTemplate = 'module:privateshop/views/templates/front/' . $this->accessType . $suffix . '.tpl';
            } else {            
                $this->display_header = false;
                $this->display_footer = false;
            }
        }

        $home = $this->context->link->getBaseLink($this->context->shop->id, true);
        if (!in_array($this->accessType, array('private_login', 'deadend'))) {
            Tools::redirect($home);
        }

        if ($this->context->cookie->__isset('access_granted') &&
            $this->context->cookie->__get('access_granted')) {
            $back = Tools::getValue('back');
            if (isset($back) && $back) {
                Tools::redirect(Tools::htmlentitiesDecodeUTF8(urldecode($back)));
            }
        }
    }

    public function initContent()
    {
        parent::initContent();
        $this->module->initPrivate();
        $field_values = $this->module->getPrivateConfigurationValues();
        $this->context->smarty->assign('field_values', $field_values);

        $this->context->smarty->assign('persist', (int) $this->persist);
        $this->context->smarty->assign('ajax_link', $this->context->link->getModuleLink($this->module->name, 'thejax', array(), true));
        $this->context->smarty->assign('signup_ajax_link', $this->context->link->getModuleLink($this->module->name, 'private', array('type' => 'private_login'), true));

        $restrict_message = Tools::safeOutput(Configuration::get(
            'PRIVATE_RESTRICT_MESSAGE',
            (int) $this->context->language->id,
            $this->context->shop->id_shop_group,
            $this->context->shop->id
        ));
        $this->context->smarty->assign('hook_create_account_form', Hook::exec('displayCustomerAccountForm'));
        if (Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=') == true) {
            $this->context->smarty->assign('link', $this->context->link);
        } else {
            $this->persist_restricted = (int) Context::getContext()->cookie->__get('privateshop_restricted');
            $this->context->smarty->assign('persist_restricted', (int) $this->persist_restricted);
            $this->context->smarty->assign('restrict_message', $restrict_message);
        }

        $this->context->smarty->assign('deadend', (('deadend' == $this->accessType)? true : false));
        $this->context->smarty->assign('PRIVATE_BLOCK', $this->context->smarty->fetch($this->module->getLocalPath() . 'views/templates/front/private-block.tpl'));

        $this->setTemplate($this->privateTemplate);
    }

    public function postProcess()
    {
        parent::postProcess();

        if (Tools::isSubmit('forgotpassword') || Tools::getIsset('reset_token')) {
            $this->persist = true;
            $this->passwordRecovery();
        } elseif (Tools::isSubmit('submitAccount') || Tools::isSubmit('submitGuestAccount')) {
            $this->processSubmitAccount();
        }
    }

    /**
     * submitCreate function for px 1.6.x.x - handle new account creation
     * @return json
     */
    public function displayAjaxSubmitCreate()
    {
        $this->module->assignDate();

        $this->module->assignCountries();

        $newsletter = Configuration::get('PS_CUSTOMER_NWSL') || (Module::isInstalled('blocknewsletter') && Module::getInstanceByName('blocknewsletter')->active);
        $this->context->smarty->assign('newsletter', $newsletter);
        $this->context->smarty->assign('optin', (bool)Configuration::get('PS_CUSTOMER_OPTIN'));

        $this->context->smarty->assign('email_create', 1);

        if (Tools::getValue('multi-shipping') == 1) {
            $this->context->smarty->assign('multi_shipping', true);
        } else {
            $this->context->smarty->assign('multi_shipping', false);
        }

        $this->context->smarty->assign('field_required', $this->context->customer->validateFieldsRequiredDatabase());

        $this->module->assignAddressFormat();

        // Call a hook to display more information on form
        $this->context->smarty->assign(array(
            'HOOK_CREATE_ACCOUNT_FORM' => Hook::exec('displayCustomerAccountForm'),
            'HOOK_CREATE_ACCOUNT_TOP' => Hook::exec('displayCustomerAccountFormTop')
        ));

        // Call a hook to display more information on form
        $this->context->smarty->assign(array(
            'PS_REGISTRATION_PROCESS_TYPE' => Configuration::get('PS_REGISTRATION_PROCESS_TYPE'),
            'genders' => Gender::getGenders(),
        ));

        $tpl = $this->context->smarty->fetch(_PS_THEME_DIR_.'authentication.tpl');
        $html = str_get_html($tpl);
        $action = $this->context->link->getModuleLink(
            $this->module->name,
            'private',
            array('type' => 'private_login'),
            true
        );
        // override default form action to private controller
        $html->getElementById('account-creation_form')->setAttribute('action', $action);
        $return = array(
            'hasError' => !empty($this->errors),
            'errors' => $this->errors,
            'page' => $html->innertext,
            'token' => Tools::getToken(false)
        );
        $this->ajaxDie(Tools::jsonEncode($return));
    }

    /**
     * processing new accounts - for ps 1.6.x.x
     */
    protected function processSubmitAccount()
    {
        $this->persist = 2;
        Hook::exec('actionBeforeSubmitAccount');
        $this->create_account = true;
        if (Tools::isSubmit('submitAccount')) {
            $this->context->smarty->assign('email_create', 1);
        }
        // New Guest customer
        if (!Tools::getValue('is_new_customer', 1) && !Configuration::get('PS_GUEST_CHECKOUT_ENABLED')) {
            $this->errors[] = $this->translations['guest_account_error'];
        }
        if (!Tools::getValue('is_new_customer', 1)) {
            $_POST['passwd'] = md5(time() . _COOKIE_KEY_);
        }
        if ($guest_email = Tools::getValue('guest_email')) {
            $_POST['email'] = $guest_email;
        }
        // Checked the user address in case he changed his email address
        if (Validate::isEmail($email = Tools::getValue('email')) && !empty($email)) {
            if (Customer::customerExists($email)) {
                $this->errors[] = $this->module->translations['duplicate_email_error'];
            }
        }

        // Preparing customer
        $customer = new Customer();
        $lastnameAddress = Tools::getValue('lastname');
        $firstnameAddress = Tools::getValue('firstname');
        $_POST['lastname'] = Tools::getValue('customer_lastname', $lastnameAddress);
        $_POST['firstname'] = Tools::getValue('customer_firstname', $firstnameAddress);
        $addresses_types = array('address');
        if (!Configuration::get('PS_ORDER_PROCESS_TYPE') && Configuration::get('PS_GUEST_CHECKOUT_ENABLED') && Tools::getValue('invoice_address')) {
            $addresses_types[] = 'address_invoice';
        }

        $error_phone = false;
        if (Configuration::get('PS_ONE_PHONE_AT_LEAST')) {
            if (Tools::isSubmit('submitGuestAccount') || !Tools::getValue('is_new_customer')) {
                if (!Tools::getValue('phone') && !Tools::getValue('phone_mobile')) {
                    $error_phone = true;
                }

            } elseif (((Configuration::get('PS_REGISTRATION_PROCESS_TYPE') && Configuration::get('PS_ORDER_PROCESS_TYPE'))
                || (Configuration::get('PS_ORDER_PROCESS_TYPE') && !Tools::getValue('email_create'))
                || (Configuration::get('PS_REGISTRATION_PROCESS_TYPE') && Tools::getValue('email_create')))
                && (!Tools::getValue('phone') && !Tools::getValue('phone_mobile'))) {
                $error_phone = true;
            }
        }

        if ($error_phone) {
            $this->errors[] = $this->module->translations['phone_error'];
        }

        $this->errors = array_unique(array_merge($this->errors, $customer->validateController()));

        // Check the requires fields which are settings in the BO
        $this->errors = $this->errors + $customer->validateFieldsRequiredDatabase();

        if (!Configuration::get('PS_REGISTRATION_PROCESS_TYPE') && !$this->ajax && !Tools::isSubmit('submitGuestAccount')) {
            if (!count($this->errors)) {
                if (Tools::isSubmit('newsletter')) {
                    $this->processCustomerNewsletter($customer);
                }

                $customer->firstname = Tools::ucwords($customer->firstname);
                $y = Tools::getValue('years');
                $m = Tools::getValue('months');
                $d = Tools::getValue('days');
                $customer->birthday = (empty($y) ? '' : (int) $y . '-' . (int) $m . '-' . (int) $d);
                if (!Validate::isBirthDate($customer->birthday)) {
                    $customer->birthday = '';
                }

                // New Guest customer
                $customer->is_guest = (Tools::isSubmit('is_new_customer') ? !Tools::getValue('is_new_customer', 1) : 0);
                $customer->active = 1;

                if (!count($this->errors)) {
                    if ($customer->add()) {
                        $multi = '';
                        if (!$customer->is_guest) {
                            if (!$this->sendConfirmationMail($customer)) {
                                $this->errors[] = $this->module->translations['email_sending_error'];
                            }
                        }

                        $this->updateContext($customer);

                        $this->context->cart->update();
                        Hook::exec('actionCustomerAccountAdd', array(
                            '_POST' => $_POST,
                            'newCustomer' => $customer,
                        ));
                        if ($this->ajax) {
                            $return = array(
                                'hasError' => !empty($this->errors),
                                'errors' => $this->errors,
                                'isSaved' => true,
                                'id_customer' => (int) $this->context->cookie->id_customer,
                                'id_address_delivery' => $this->context->cart->id_address_delivery,
                                'id_address_invoice' => $this->context->cart->id_address_invoice,
                                'token' => Tools::getToken(false),
                            );
                            $this->ajaxDie(Tools::jsonEncode($return));
                        }
                        $restrict_state = (int) Configuration::get('PRIVATE_SIGNUP_RESTRICT');
                        if ($restrict_state > 0) {
                            //Let make cookie so we know if restricted is valid
                            $this->context->cookie->privateshop_restricted = true;
                            $this->context->cookie->write();
                        } else {
                            if (($back = Tools::getValue('back')) && $back == Tools::secureReferrer($back)) {
                                Tools::redirect(html_entity_decode($back));
                            }
                            // redirection: if cart is not empty : redirection to the cart
                            if (count($this->context->cart->getProducts(true)) > 0) {
                                Tools::redirect('index.php?controller=order' . ($multi = (int) Tools::getValue('multi-shipping') ? '&multi-shipping=' . $multi : ''));
                            } else {
                                Tools::redirect('index.php?controller=' . (($this->authRedirection !== false) ? urlencode($this->authRedirection) : 'my-account'));
                            }
                        }
                    } else {
                        $this->errors[] = $this->module->translations['account_creation_error'];
                    }
                }
            }
        } else {
            $_POST['lastname'] = $lastnameAddress;
            $_POST['firstname'] = $firstnameAddress;
            $post_back = $_POST;
            // Preparing addresses
            foreach ($addresses_types as $addresses_type) {
                $$addresses_type = new Address();
                $$addresses_type->id_customer = 1;

                if ($addresses_type == 'address_invoice') {
                    foreach ($_POST as $key => &$post) {
                        if ($tmp = Tools::getValue($key . '_invoice')) {
                            $post = $tmp;
                        }
                    }
                }

                $this->errors = array_unique(array_merge($this->errors, $$addresses_type->validateController()));
                if ($addresses_type == 'address_invoice') {
                    $_POST = $post_back;
                }

                if (!($country = new Country($$addresses_type->id_country)) || !Validate::isLoadedObject($country)) {
                    $this->errors[] = $this->module->translations['country_error'];
                }

                if (!$country->active) {
                    $this->errors[] = $this->module->translations['country_deactive'];
                }

                $postcode = $$addresses_type->postcode;
                /* Check zip code format */
                if ($country->zip_code_format && !$country->checkZipCode($postcode)) {
                    $this->errors[] = sprintf(
                        $this->module->translations['invalid_zipcode'],
                        str_replace('C', $country->iso_code, str_replace('N', '0', str_replace('L', 'A', $country->zip_code_format)))
                    );
                } elseif (empty($postcode) && $country->need_zip_code) {
                    $this->errors[] = $this->module->translations['zipcode_required'];
                } elseif ($postcode && !Validate::isPostCode($postcode)) {
                    $this->errors[] = $this->module->translations['zicode_invalid'];
                }

                if ($country->need_identification_number && (!Tools::getValue('dni') || !Validate::isDniLite(Tools::getValue('dni')))) {
                    $this->errors[] = $this->module->translations['identificaion_invalid'];
                } elseif (!$country->need_identification_number) {
                    $$addresses_type->dni = null;
                }

                if (Tools::isSubmit('submitAccount') || Tools::isSubmit('submitGuestAccount')) {
                    if (!($country = new Country($$addresses_type->id_country, Configuration::get('PS_LANG_DEFAULT'))) || !Validate::isLoadedObject($country)) {
                        $this->errors[] = $this->module->translations['invalid_country'];
                    }
                }

                $contains_state = isset($country) && is_object($country) ? (int) $country->contains_states : 0;
                $id_state = isset($$addresses_type) && is_object($$addresses_type) ? (int) $$addresses_type->id_state : 0;
                if ((Tools::isSubmit('submitAccount') || Tools::isSubmit('submitGuestAccount')) && $contains_state && !$id_state) {
                    $this->errors[] = $this->module->translations['state_required'];
                }
            }
        }
        $_m = Tools::getValue('months');
        $_d = Tools::getValue('days');
        $_y = Tools::getValue('years');
        //if (!@checkdate($_m, $_d, $_y) && !($_m == '' && $_d == '' && $_y == '')) {
        //  $this->errors[] = Tools::displayError('Invalid date of birth');
        //}

        if (!count($this->errors)) {
            if (Customer::customerExists(Tools::getValue('email'))) {
                $this->errors[] = $this->module->translations['account_exists'];
            }
            if (Tools::isSubmit('newsletter')) {
                $this->processCustomerNewsletter($customer);
            }

            $customer->birthday = ($_y ? '' : (int) $_y . '-' . (int) $_m . '-' . (int) $_d);
            if (!Validate::isBirthDate($customer->birthday)) {
                $customer->birthday = '';
            }

            if (!count($this->errors)) {
                $customer->active = 1;
                // New Guest customer
                if (Tools::isSubmit('is_new_customer')) {
                    $customer->is_guest = !Tools::getValue('is_new_customer', 1);
                } else {
                    $customer->is_guest = 0;
                }
                if (!$customer->add()) {
                    $this->errors[] = $this->module->translations['account_creation_error'];
                } else {
                    foreach ($addresses_types as $addresses_type) {
                        $$addresses_type->id_customer = (int) $customer->id;
                        if ($addresses_type == 'address_invoice') {
                            foreach ($_POST as $key => &$post) {
                                if ($tmp = Tools::getValue($key . '_invoice')) {
                                    $post = $tmp;
                                }
                            }
                        }

                        $this->errors = array_unique(array_merge($this->errors, $$addresses_type->validateController()));
                        if ($addresses_type == 'address_invoice') {
                            $_POST = $post_back;
                        }
                        if (!count($this->errors) && (Configuration::get('PS_REGISTRATION_PROCESS_TYPE') || $this->ajax || Tools::isSubmit('submitGuestAccount')) && !$$addresses_type->add()) {
                            $this->errors[] = $this->module->translations['address_error'];
                        }
                    }
                    if (!count($this->errors)) {
                        if (!$customer->is_guest) {
                            $this->context->customer = $customer;
                            $customer->cleanGroups();
                            // we add the guest customer in the default customer group
                            $customer->addGroups(array((int) Configuration::get('PS_CUSTOMER_GROUP')));
                            if (!$this->sendConfirmationMail($customer)) {
                                $this->errors[] = $this->module->translations['email_sending_error'];
                            }
                        } else {
                            $customer->cleanGroups();
                            // we add the guest customer in the guest customer group
                            $customer->addGroups(array((int) Configuration::get('PS_GUEST_GROUP')));
                        }
                        $this->updateContext($customer);
                        $this->context->cart->id_address_delivery = (int) Address::getFirstCustomerAddressId((int) $customer->id);
                        $this->context->cart->id_address_invoice = (int) Address::getFirstCustomerAddressId((int) $customer->id);
                        // if (isset($address_invoice) && Validate::isLoadedObject($address_invoice))
                        //  $this->context->cart->id_address_invoice = (int)$address_invoice->id;

                        if ($this->ajax && Configuration::get('PS_ORDER_PROCESS_TYPE')) {
                            $delivery_option = array((int) $this->context->cart->id_address_delivery => (int) $this->context->cart->id_carrier . ',');
                            $this->context->cart->setDeliveryOption($delivery_option);
                        }

                        // If a logged guest logs in as a customer, the cart secure key was already set and needs to be updated
                        $this->context->cart->update();

                        // Avoid articles without delivery address on the cart
                        $this->context->cart->autosetProductAddress();

                        Hook::exec('actionCustomerAccountAdd', array(
                            '_POST' => $_POST,
                            'newCustomer' => $customer,
                        ));
                        if ($this->ajax) {
                            $return = array(
                                'hasError' => !empty($this->errors),
                                'errors' => $this->errors,
                                'isSaved' => true,
                                'id_customer' => (int) $this->context->cookie->id_customer,
                                'id_address_delivery' => $this->context->cart->id_address_delivery,
                                'id_address_invoice' => $this->context->cart->id_address_invoice,
                                'token' => Tools::getToken(false),
                            );
                            $this->ajaxDie(Tools::jsonEncode($return));
                        }
                    }
                }
            }
        }

        if (count($this->errors)) {
            //for retro compatibility to display guest account creation form on authentication page
            if (Tools::getValue('submitGuestAccount')) {
                $_GET['display_guest_checkout'] = 1;
            }

            if (!Tools::getValue('is_new_customer')) {
                unset($_POST['passwd']);
            }
            if ($this->ajax) {
                $return = array(
                    'hasError' => !empty($this->errors),
                    'errors' => $this->errors,
                    'isSaved' => false,
                    'id_customer' => 0,
                );
                $this->ajaxDie(Tools::jsonEncode($return));
            }
            $this->context->smarty->assign('account_error', $this->errors);
        }
    }

    protected function passwordRecovery()
    {
        $this->persist = 0;
        if (Tools::getValue('private_pass_recovery')) {
            if (!($email = trim(Tools::getValue('email'))) || !Validate::isEmail($email)) {
                $this->persist = true;
                $this->errors[] = $this->module->translations['invalid_email'];
            } else {
                $customer = new Customer();
                $customer->getByemail($email);
                if (!Validate::isLoadedObject($customer)) {
                    $this->persist = true;
                    $this->errors[] = $this->module->translations['no_account_registered'];
                } elseif (!$customer->active) {
                    $this->persist = true;
                    $this->errors[] = $this->module->translations['cannot_regen_pwd'];
                } elseif ((strtotime($customer->last_passwd_gen . '+' . ($min_time = (int) Configuration::get('PS_PASSWD_TIME_FRONT')) . ' minutes') - time()) > 0) {
                    $this->persist = true;
                    $this->errors[] = sprintf($this->module->translations['gen_pwd_after_x'], (int) $min_time);
                } else {
                    if (true === (bool) Tools::version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                        if (!$customer->hasRecentResetPasswordToken()) {
                            $customer->stampResetPasswordToken();
                            $customer->update();
                        }
                        $pass_reset_link = $this->context->link->getPageLink(
                            'password',
                            true,
                            null,
                            array(
                                'token' => $customer->secure_key,
                                'id_customer' => (int) $customer->id,
                                'reset_token' => $customer->reset_password_token,
                            )
                        );
                    } else {
                        $pass_reset_link = $this->context->link->getPageLink(
                            'password',
                            true,
                            null,
                            array(
                                'persist' => 1,
                                'token' => $customer->secure_key,
                                'id_customer' => (int) $customer->id,
                            )
                        );
                    }

                    $mail_params = array(
                        '{email}' => $customer->email,
                        '{lastname}' => $customer->lastname,
                        '{firstname}' => $customer->firstname,
                        '{url}' => $pass_reset_link,
                    );

                    if (Mail::Send(
                        $this->context->language->id,
                        'password_query',
                        Mail::l('Password query confirmation'),
                        $mail_params,
                        $customer->email,
                        $customer->firstname . ' ' . $customer->lastname,
                        null,
                        null,
                        null,
                        null,
                        _PS_MAIL_DIR_,
                        false,
                        $this->context->shop->id
                    )) {
                        $this->persist = true;
                        $this->context->smarty->assign(array(
                            'confirmation' => 2,
                            'customer_email' => $customer->email,
                        ));
                    } else {
                        $this->persist = true;
                        $this->errors[] = $this->module->translations['email_sending_error'];
                    }
                }
            }
        } elseif (($token = Tools::getValue('token')) && ($id_customer = (int) Tools::getValue('id_customer'))) {
            $email = PrivateAcces::getCustomerEmail($id_customer, $token);
            if ($email) {
                $customer = new Customer();
                $customer->getByemail($email);
                if (!Validate::isLoadedObject($customer)) {
                    $this->errors[] = $this->module->translations['account_404'];
                } elseif (!$customer->active) {
                    $this->errors[] = $this->module->translations['cannot_regen_pwd'];
                } elseif ((strtotime($customer->last_passwd_gen . '+' . (int) Configuration::get('PS_PASSWD_TIME_FRONT') . ' minutes') - time()) > 0) {
                    Tools::redirect('index.php?controller=authentication&error_regen_pwd');
                } else {
                    $customer->passwd = Tools::encrypt($password = Tools::passwdGen(MIN_PASSWD_LENGTH));
                    $customer->last_passwd_gen = date('Y-m-d H:i:s', time());
                    if ($customer->update()) {
                        Hook::exec('actionPasswordRenew', array('customer' => $customer, 'password' => $password));
                        $mail_params = array(
                            '{email}' => $customer->email,
                            '{lastname}' => $customer->lastname,
                            '{firstname}' => $customer->firstname,
                            '{passwd}' => $password,
                        );
                        if (Mail::Send(
                            $this->context->language->id,
                            'password',
                            Mail::l('Your new password'),
                            $mail_params,
                            $customer->email, $customer->firstname . ' ' . $customer->lastname,
                            null,
                            null,
                            null,
                            null,
                            _PS_MODULE_DIR_ . 'privateshop/mails/', false, $this->context->shop->id
                        )) {
                            $this->context->smarty->assign(array('confirmation' => 1, 'customer_email' => $customer->email));
                        } else {
                            $this->errors[] = $this->module->translations['email_sending_error'];
                        }
                    } else {
                        $this->errors[] = $this->module->translations['pwd_sending_failed'];
                    }
                }
            } else {
                $this->errors[] = $this->module->translations['invalid_pwd_data'];
            }
        } elseif (Tools::getValue('token') || Tools::getValue('id_customer')) {
            $this->errors[] = $this->module->translations['invalid_pwd_data'];
        }
    }

    /**
     * Process the newsletter settings and set the customer infos.
     *
     * @param Customer $customer Reference on the customer Object.
     *
     * @note At this point, the email has been validated.
     */
    protected function processCustomerNewsletter(&$customer)
    {
        $blocknewsletter = Module::isInstalled('blocknewsletter') && $module_newsletter = Module::getInstanceByName('blocknewsletter');
        if ($blocknewsletter && $module_newsletter->active && !Tools::getValue('newsletter')) {
            require_once _PS_MODULE_DIR_.'blocknewsletter/blocknewsletter.php';
            if (is_callable(array($module_newsletter, 'isNewsletterRegistered')) && $module_newsletter->isNewsletterRegistered(Tools::getValue('email')) == Blocknewsletter::GUEST_REGISTERED) {
                /* Force newsletter registration as customer as already registred as guest */
                $_POST['newsletter'] = true;
            }
        }

        if (Tools::getValue('newsletter')) {
            $customer->newsletter = true;
            $customer->ip_registration_newsletter = pSQL(Tools::getRemoteAddr());
            $customer->newsletter_date_add = pSQL(date('Y-m-d H:i:s'));
            /** @var Blocknewsletter $module_newsletter */
            if ($blocknewsletter && $module_newsletter->active) {
                $module_newsletter->confirmSubscription(Tools::getValue('email'));
            }
        }
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        if ($this->header_footer) {
            $this->addCss($this->module->getLocalPath() . 'views/css/private_block.css');
            $this->addJS($this->module->getLocalPath() . 'views/js/private-login.js');
            $field_values = $this->module->getPrivateConfigurationValues();
            if (isset($field_values) AND isset($field_values['bg_type'])) {
                if ($field_values['bg_type'] == 'background-video' AND isset($field_values['bg_video'])) {
                    $this->addJS($this->module->getLocalPath() . 'views/js/jquery.tubular.1.0.js');
                }
            }
        }
    }

    private function sendConfirmationMail(Customer $customer)
    {
        if ($customer->is_guest || !Configuration::get('PS_CUSTOMER_CREATION_EMAIL')) {
            return true;
        }
        return Mail::Send(
            $this->context->language->id,
            'account',
            $this->module->translations['welcome'],
            array(
                '{firstname}' => $customer->firstname,
                '{lastname}' => $customer->lastname,
                '{email}' => $customer->email,
            ),
            $customer->email,
            $customer->firstname . ' ' . $customer->lastname,
            null,
            null,
            null,
            null,
            _PS_MAIL_DIR_,
            false,
            $this->context->shop->id
        );
    }

    /**
     * Update context after customer creation - 1.6.x.x
     * @param Customer $customer Created customer
     */
    protected function updateContext(Customer $customer)
    {
        $this->context->customer = $customer;
        $this->context->smarty->assign('confirmation', 1);
        $this->context->cookie->id_customer = (int)$customer->id;
        $this->context->cookie->customer_lastname = $customer->lastname;
        $this->context->cookie->customer_firstname = $customer->firstname;
        $this->context->cookie->passwd = $customer->passwd;
        $this->context->cookie->logged = 1;
        // if register process is in two steps, we display a message to confirm account creation
        if (!Configuration::get('PS_REGISTRATION_PROCESS_TYPE')) {
            $this->context->cookie->account_created = 1;
        }
        $customer->logged = 1;
        $this->context->cookie->email = $customer->email;
        $this->context->cookie->is_guest = !Tools::getValue('is_new_customer', 1);
        // Update cart address
        $this->context->cart->secure_key = $customer->secure_key;
    }
}
