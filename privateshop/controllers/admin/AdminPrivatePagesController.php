<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file.
 * You are not authorized to modify, copy or redistribute this file.
 * Permissions are reserved by FME Modules.
 *
 *  @author    FMM Modules
 *  @copyright FME Modules 2020
 *  @license   Single domain
 */

class AdminPrivatePagesController extends ModuleAdminController
{
    public function init()
    {
        parent::init();
        if (!$this->ajax) {
            Tools::redirectAdmin(Context::getContext()->link->getAdminLink('AdminModules') . '&configure=' . $this->module->name);
        }
    }

    public function ajaxProcessFetchProducts()
    {
        PrivatePages::searchProduct();
    }

    public function ajaxProcessGetProducts()
    {
        $query = Tools::replaceAccentedChars(urldecode(Tools::getValue('q')));
        $searchResults = '';
        if (!empty($query) && $query) {
            $searchResults = Search::find((int)(Tools::getValue('id_lang')), $query, 1, 10, 'position', 'desc', true);
        }
        die(Tools::jsonEncode($searchResults));
    }
}
