<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file.
 * You are not authorized to modify, copy or redistribute this file.
 * Permissions are reserved by FME Modules.
 *
 *  @author    FMM Modules
 *  @copyright FME Modules 2020
 *  @license   Single domain
 */

$sql = array();

$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'privateshop_shop';

$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'privateshop_urls';

$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'privateshop_urls_restricted';

$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'privateshop_protecetd_pages';

$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'privateshop_protecetd_pages_lang';

$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'privateshop_protecetd_pages_shop';

$sql[] = 'DROP TABLE IF EXISTS '._DB_PREFIX_.'privateshop_protecetd_pages_condition';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
