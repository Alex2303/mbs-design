<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file.
 * You are not authorized to modify, copy or redistribute this file.
 * Permissions are reserved by FME Modules.
 *
 *  @author    FMM Modules
 *  @copyright FME Modules 2020
 *  @license   Single domain
 */

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'privateshop_shop`(
        `id_shop`                           		int(10) unsigned NOT NULL,
        `id_group`                          		int(10) unsigned NOT NULL,
        PRIMARY KEY                         		(`id_shop`, `id_group`))
        ENGINE=' . _MYSQL_ENGINE_ . '       		DEFAULT CHARSET=utf8';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'privateshop_urls`(
        `id_privateshop_urls`               		int(11) NOT NULL auto_increment,
        `url`                               		varchar(255) NOT NULL,
        PRIMARY KEY                         		(`id_privateshop_urls`))
        ENGINE=' . _MYSQL_ENGINE_ . '       		DEFAULT CHARSET=utf8';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'privateshop_urls_restricted`(
        `id_privateshop_urls_restricted`    		int(11) NOT NULL auto_increment,
        `url`                               		varchar(255) NOT NULL,
        PRIMARY KEY                         		(`id_privateshop_urls_restricted`))
        ENGINE=' . _MYSQL_ENGINE_ . '       		DEFAULT CHARSET=utf8';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' ._DB_PREFIX_.'privateshop_protecetd_pages` (
        `id_privateshop_protecetd_pages`            INT(11) NOT NULL AUTO_INCREMENT,
        `passwd`                                    VARCHAR(255),
        `active`                                    TINYINT(2),
        `date_add`                                  DATE,
        PRIMARY KEY                                 (`id_privateshop_protecetd_pages`)
        ) ENGINE='._MYSQL_ENGINE_.'                 DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' ._DB_PREFIX_.'privateshop_protecetd_pages_lang` (
        `id_privateshop_protecetd_pages`            INT(11) NOT NULL,
        `id_lang`                                   INT(11) NOT NULL,
        `message`                                   TEXT,
        PRIMARY KEY                                 (`id_privateshop_protecetd_pages`, `id_lang`)
        ) ENGINE='._MYSQL_ENGINE_.'                 DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' ._DB_PREFIX_.'privateshop_protecetd_pages_shop` (
        `id_privateshop_protecetd_pages`            INT(11) NOT NULL,
        `id_shop`                                   INT(11) NOT NULL,
        `id_shop_group`                             INT(11) NOT NULL,
        PRIMARY KEY                                 (`id_privateshop_protecetd_pages`, `id_shop`)
        ) ENGINE='._MYSQL_ENGINE_.'                 DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' ._DB_PREFIX_.'privateshop_protecetd_pages_condition` (
        `id_privateshop_protecetd_pages_condition`  INT(11) NOT NULL AUTO_INCREMENT,
        `id_privateshop_protecetd_pages`            INT(11) NOT NULL,
        `type`                                      VARCHAR(20) NOT NULL,
        `value`                                     INT(11) NOT NULL,
        PRIMARY KEY                                 (`id_privateshop_protecetd_pages_condition`)
        ) ENGINE='._MYSQL_ENGINE_.'                 DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
