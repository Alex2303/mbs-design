<?php
/**
* DISCLAIMER
*
* Do not edit or add to this file.
* You are not authorized to modify, copy or redistribute this file.
* Permissions are reserved by FME Modules.
*
*  @author    FMM Modules
*  @copyright FME Modules 2020
*  @license   Single domain
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_7_0($module)
{
    $result = true;
    $module->setShopIdx();
    $result &= Configuration::updateValue(
        'PRIVATE_SHOW_HEADER_FOOTER',
        0,
        null,
        Context::getContext()->id_shop_group,
        Context::getContext()->id_shop
    );


    $result &= $module->generatePrivatePageTables();

    $result &= $module->installTab('AdminPrivatePages', 'Private Pages');
    
    // clear cache after removing overrides
    if ($module->uninstallOverrides()) {
        $module->removeCache();
        $result &= true;
    }
    
    return $result;
}